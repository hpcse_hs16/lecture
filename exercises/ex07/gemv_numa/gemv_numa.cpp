#include <matrix.hpp>
#include <vector>

#include <random>
#include <algorithm>
#include <iostream>
#include <thread>
#include <cassert>

#include <no_init_vector.hpp>

template <typename Vector, typename Matrix>
void gemv_serial(Vector& y, double alpha, Matrix const& A, Vector const& x, double beta)
{
    assert(y.size() == num_rows(A));
    assert(x.size() == num_cols(A));
    std::size_t const cols = num_cols(A);
    std::size_t const rows = num_rows(A);
    for(std::size_t i=0; i < rows; ++i)
    {
        double yl = beta*y[i];
        for(std::size_t j=0; j < cols; ++j)
            yl += alpha * A(i,j) * x[j];
        y[i] = yl;
    }
}


template <typename Vector, typename Matrix>
void gemv_parallel(Vector& y, double alpha, Matrix const& A, Vector const& x, double beta, unsigned int const nthreads)
{
    assert(y.size() == num_rows(A));
    assert(x.size() == num_cols(A));
    std::size_t const cols = num_cols(A);
    std::size_t const rows = num_rows(A);

    auto f = [alpha, beta, rows, cols, nthreads, &y, &A, &x](unsigned int t){
                std::size_t const begin = (rows*t)/nthreads;
                std::size_t const end   = (rows*(t+1))/nthreads;
                for(std::size_t i = begin; i < end; ++i)
                {
                    double yl = beta*y[i];
                    for(std::size_t j=0; j < cols; ++j)
                        yl += alpha * A(i,j) * x[j];
                    y[i] = yl;
                }
            };

    // Bring in the clowns ... aeh spawn the threads!
    std::vector<std::thread> threads;
    for(unsigned int t=1; t < nthreads; ++t)
        threads.push_back( std::thread(f,t));

    // Let's do a little work in the main thread
    f(0);

    for(auto& t : threads)
        t.join();
}

void numa_init(double* begin, double* end, double value, unsigned int const nthreads)
{
    std::size_t const dist = end-begin;

    auto f = [begin,end,value,dist,nthreads](unsigned int t){
                    double* it         = begin+(dist*t)/nthreads;
                    double* const tend = begin+(dist*(t+1))/nthreads;
                    for(; it != tend; ++it)
                        *it = value;
                };

    // Spawn threads
    std::vector<std::thread> threads;
    for(unsigned int t=1; t < nthreads; ++t)
        threads.push_back( std::thread(f,t));

    // main thread
    f(0);

    for(std::thread& t : threads)
        t.join();
}

template <typename Vector, typename Matrix>
void check_gemv(Vector y, double alpha, Matrix const& A, Vector const& x, double beta, unsigned int const nthreads)
{
    Vector yc(y);

    std::cout << "Checking parallel against serial implementation ...";
    std::cout.flush();

    gemv_serial  (y,  alpha, A, x, beta);
    gemv_parallel(yc, alpha, A, x, beta, nthreads);

    // Check results
    bool error = false;
    for(std::size_t i =0; i < y.size(); ++i)
        if(std::abs(y[i]/yc[i] - 1.0) > 1e-5)
        {
            error = true;
            std::cout << std::endl << "ERROR: y[" << i << "] mismatch: "<< y[i] <<" != " << yc[i];
        }

    if(!error)
        std::cerr << " OK" << std::endl;
    else
        std::cerr << std::endl <<"ERROR: serial and parallel gemv mismatch!" << std::endl;
}


// Warning: Will alter y!
template <typename Vector, typename Matrix>
void benchmark_gemv(Vector& y, double alpha, Matrix const& A, Vector const& x, double beta, unsigned int const nthreads)
{
    std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
    start = std::chrono::high_resolution_clock::now();
    if(nthreads == 1)
        gemv_serial(y, 1.0, A, x, 1.0);
    else
        gemv_parallel(y, 1.0, A, x, 1.0, nthreads);
    end   = std::chrono::high_resolution_clock::now();
    double t = std::chrono::nanoseconds(end - start).count()*1e-9;

    std::cout << "threads: "<< nthreads << " matrix_size: " << num_rows(A) << " time: " << t << std::endl;
}

int main(int argc, char* argv[])
{
    if(argc != 4)
    {
        std::cerr << "Usage: " << argv[0] << " <matrix_size> <threads> <number_of_runs>" << std::endl;
        return -1;
    }
    std::size_t const  m = atoi(argv[1]);
    std::size_t const  nthreads = atoi(argv[2]);
    unsigned int const nruns = atoi(argv[3]);


    hpcse::no_init_vector<double> y(m);
    hpcse::no_init_vector<double> x(m);
    hpcse::matrix<double, hpcse::row_major, hpcse::no_init_vector<double, hpcse::aligned_allocator<double,4096> > > A(m,m);

    numa_init(y.begin(), y.end(), 0.0, nthreads);
    numa_init(A.data(), A.data()+(num_rows(A)*num_cols(A)), 0.0, nthreads);

    // Initialize with uniform random numbers
    std::seed_seq seq{3000};
    std::mt19937 rng(seq);
    std::uniform_real_distribution<double> rnd(0.,1.);

    std::generate(y.begin(), y.end(), [&rng,&rnd](){ return rnd(rng); } );
    std::generate(x.begin(), x.end(), [&rng,&rnd](){ return rnd(rng); } );
    std::generate_n(A.data(), num_rows(A)*num_cols(A), [&rng,&rnd](){ return rnd(rng); } );


    check_gemv(y, 1.0, A, x, 1.0, nthreads);

    for(unsigned int i =0; i < nruns; ++i)
        benchmark_gemv(y, 1.0, A, x, 1.0, nthreads);

    return 0;
}
