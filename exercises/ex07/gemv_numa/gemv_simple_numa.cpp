#include <random>
#include <algorithm>
#include <iostream>
#include <thread>

void gemv_serial(double* y, double alpha, double const* A, double const* x, double beta, unsigned int const m)
{
    for(std::size_t i=0; i < m; ++i)
    {
        double yl = beta*y[i];
        for(std::size_t j=0; j < m; ++j)
            yl += alpha * A[i*m+j] * x[j];
        y[i] = yl;
    }
}


void gemv_parallel(double* y, double alpha, double const* A, double const* x, double beta, unsigned int const m, unsigned int const nthreads)
{
    std::vector<std::thread> threads;
    for(unsigned int t=0; t < nthreads; ++t)
        threads.push_back(
          std::thread(
            [t, alpha, beta, m, nthreads, &y, &A, &x](){
                std::size_t const begin = (m*t)/nthreads;
                std::size_t const end   = (m*(t+1))/nthreads;
                for(std::size_t i = begin; i < end; ++i)
                {
                    double yl = beta*y[i];
                    for(std::size_t j=0; j < m; ++j)
                        yl += alpha * A[i*m+j] * x[j];
                    y[i] = yl;
                }
            }
          )
        );

    for(auto& t : threads)
        t.join();
}

void numa_init(double* begin, double* end, double value, unsigned int const nthreads)
{
    std::size_t const dist = end-begin;

    auto f = [begin,end,value,dist,nthreads](unsigned int t){
                    double* it         = begin+(dist*t)/nthreads;
                    double* const tend = begin+(dist*(t+1))/nthreads;
                    for(; it != tend; ++it)
                        *it = value;
                };

    // Spawn threads
    std::vector<std::thread> threads;
    for(unsigned int t=1; t < nthreads; ++t)
        threads.push_back( std::thread(f,t));

    // main thread
    f(0);

    for(std::thread& t : threads)
        t.join();
}


void check_gemv(double* y, double alpha, double const* A, double const* x, double beta, unsigned int const m, unsigned int const nthreads)
{
    std::cout << "Checking parallel against serial implementation ...";
    std::cout.flush();

    double *yc = new double[m];
    for(std::size_t i=0; i < m; ++i)
        yc[i] = y[i];

    gemv_serial  (y,  alpha, A, x, beta, m);
    gemv_parallel(yc, alpha, A, x, beta, m, nthreads);

    // Check results
    bool error = false;
    for(std::size_t i =0; i < m; ++i)
        if(std::abs(y[i]/yc[i] - 1.0) > 1e-5)
        {
            error = true;
            std::cout << std::endl << "ERROR: y[" << i << "] mismatch: "<< y[i] <<" != " << yc[i];
        }

    delete[] yc;

    if(!error)
        std::cerr << " OK" << std::endl;
    else
        std::cerr << std::endl <<"ERROR: serial and parallel gemv mismatch!" << std::endl;
}


// Warning: Will alter y!
void benchmark_gemv(double* y, double alpha, double const* A, double const* x, double beta, unsigned int const m, unsigned int const nthreads)
{
    std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
    start = std::chrono::high_resolution_clock::now();
    if(nthreads == 1)
        gemv_serial(y, 1.0, A, x, 1.0, m);
    else
        gemv_parallel(y, 1.0, A, x, 1.0, m, nthreads);
    end   = std::chrono::high_resolution_clock::now();
    double t = std::chrono::nanoseconds(end - start).count()*1e-9;

    std::cout << "threads: "<< nthreads << " matrix_size: " << m << " time: " << t << std::endl;
}

int main(int argc, char* argv[])
{
    if(argc != 4)
    {
        std::cerr << "Usage: " << argv[0] << " <matrix_size> <threads> <number_of_runs>" << std::endl;
        return -1;
    }
    std::size_t const  m = atoi(argv[1]);
    std::size_t const  nthreads = atoi(argv[2]);
    unsigned int const nruns = atoi(argv[3]);


    double* y = new double[m];
    double* x = new double[m];
    double* A = new double[m*m];

    numa_init(y, y+m,0.0, nthreads);
    numa_init(A, A+m*m,0.0, nthreads);

    // Initialize with uniform random numbers
    std::seed_seq seq{3000};
    std::mt19937 rng(seq);
    std::uniform_real_distribution<double> rnd(0.,1.);

    std::generate_n(y, m, [&rng,&rnd](){ return rnd(rng); } );
    std::generate_n(x, m, [&rng,&rnd](){ return rnd(rng); } );
    std::generate_n(A, m*m, [&rng,&rnd](){ return rnd(rng); } );


    check_gemv(y, 1.0, A, x, 1.0, m, nthreads);

    for(unsigned int i =0; i < nruns; ++i)
        benchmark_gemv(y, 1.0, A, x, 1.0, m, nthreads);

    delete[] A;
    delete[] x;
    delete[] y;
    return 0;
}
