#ifndef HPCSE_NO_INIT_VECTOR_HPP
#define HPCSE_NO_INIT_VECTOR_HPP

#include <algorithm>
#include <type_traits>
#include <aligned_allocator.hpp>

namespace hpcse {

template <typename T, typename Allocator = hpcse::aligned_allocator<T,64> >
class no_init_vector
{
  public:
    typedef T               value_type;
    typedef std::size_t     size_type;
    typedef std::ptrdiff_t  difference_type;

    typedef value_type&       reference;
    typedef value_type const& const_reference;

    typedef value_type*          iterator;
    typedef value_type const*    const_iterator;

    explicit no_init_vector(size_type n, value_type ignored = value_type(), Allocator alloc = Allocator())
    : data_(0), size_(0), alloc_(alloc)
    {
        static_assert(std::is_fundamental<value_type>::value, "T must be a fundamental type!");
        // We assume fundamental types, because we do not have to call their
        // destructor in case of an exception. Just deallocation is fine.
        data_ = alloc_.allocate(n);
        size_ = n;
    }

    no_init_vector(no_init_vector const& v)
    : data_(0), size_(0), alloc_(v.alloc_)
    {
        using std::uninitialized_copy;
        data_ = alloc_.allocate(v.size_);
        size_ = v.size_;
        // uninitalized_copy will take care of exception safe copy.
        // size needs to be set before copy,
        // since it has to be passed to deallocate on exception.
        // (not needed anymore since we assume fundamental types only).
        uninitialized_copy(v.begin(), v.end(), this->begin());
    }

    ~no_init_vector()
    {
        alloc_.deallocate(data_,size_);
    }

    value_type&         operator[](size_type i) { return data_[i]; }
    value_type const&   operator[](size_type i) const { return data_[i]; }

    iterator            begin() { return data_; }
    iterator            end()   { return data_+size_; }
    const_iterator      begin() const { return data_; }
    const_iterator      end()   const { return data_+size_; }

    value_type*         data() { return data_; }
    value_type const*   data() const { return data_; }

    size_type           size() const { return size_; }
    bool                empty() const { return size_ == 0; }

    value_type&         front() { return data_[0]; }
    value_type const&   front() const { return data_[0]; }


    friend void swap(no_init_vector& a, no_init_vector& b)
    {
        using std::swap;
        swap(a.size_,b.size_);
        swap(a.data_,b.data_);
    }
  private:
    value_type* data_;
    size_type size_;
    Allocator alloc_;
};


} // end namespace hpcse
#endif // HPCSE_NO_INIT_VECTOR_HPP
