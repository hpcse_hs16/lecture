// Example codes for HPC course
// (c) 2016 Donjan Rodic, ETH Zurich

#include "matrix.hpp"

#include <cblas.h>
//~ #include <mkl.h>
#include <omp.h>
#include <x86intrin.h>

#include <iostream>
#include <iomanip>
#include <cassert>
#include <algorithm>
#include <list>
#include <map>
#include <chrono>
#include <cmath>

typedef hpcse::matrix<double,hpcse::row_major> matrix_type;

// WARNING: all security and boundary checks have been removed to facilitate code clarity
//          REUSE AT YOUR OWN RISK

////////////////////////////////////////////////////////////////////////////////

void print_matrix(const matrix_type& m, size_t view = std::numeric_limits<size_t>::max()) {
    std::cout << std::endl;
    for(size_t i = 0; i < std::min(num_rows(m), (uint)view); ++i) {
        for(size_t j = 0; j < std::min(num_cols(m), (uint)view); ++j)
            std::cout << ' ' <<  m(i,j);
        std::cout << std::endl;
    }
}

// row major square matrix as raw array
template<typename T>
void print_matrix(const T* m, const size_t size, size_t view = std::numeric_limits<size_t>::max()) {
    std::cout << std::endl;
    for(size_t i = 0; i < std::min(size, view); ++i) {
        for(size_t j = 0; j < std::min(size, view); ++j)
            std::cout << ' ' <<  m[i*size + j];
        std::cout << std::endl;
    }
}

template<typename T>
void check(T alg,
           const matrix_type& a, const matrix_type& b, const matrix_type& ref,
           const double tolerance = 1e-8
    ) {
    matrix_type c(num_rows(a),num_cols(b));
    alg(a,b,c);
    for(size_t i = 0; i < num_rows(a); ++i)
        for(size_t j = 0; j < num_cols(b); ++j)  {
            if(std::abs(c(i,j) - ref(i,j))/ref(i,j) > tolerance ) {
                const double diff =  std::abs(100*(c(i,j) - ref(i,j))/ref(i,j));
                std::cerr << "WARNING: (" << i << "," << j
                          << ") has a difference of " << diff << "%" << std::endl;
            }
        }
}

template<typename T>
std::pair<double, double> benchmark(T alg,
             const matrix_type& a, const matrix_type& b, matrix_type& c,
             const size_t n = 7
    ) {
    double sec, sum = 0, sum2 = 0;
    std::chrono::time_point<std::chrono::high_resolution_clock> start, stop;
    for(size_t i = 0; i < n; ++i) {
        start = std::chrono::high_resolution_clock::now();
        alg(a,b,c);
        stop = std::chrono::high_resolution_clock::now();
        sec = std::chrono::duration<double>(stop-start).count();
        sum += sec;
        sum2 += sec*sec;
    }
    double dev = 0;
    if(n > 1)
        dev = std::sqrt(std::abs(n*sum2 - sum*sum) / (n*(n-1)));

    return std::make_pair(sum/n, dev);
}

////////////////////////////////////////////////////////////////////////////////
// naive implementation

void gemm(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){
    for(size_t i=0; i < num_rows(a); ++i) {
        for(size_t j=0; j < num_cols(b); ++j) {
            for(size_t k=0; k < num_cols(a); ++k) {
                c(i,j) += a(i,k) * b(k,j);
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// do away with function calling overhead

void gemm_array(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){
    const double* A = a.data();
    const double* B = b.data();
    double* C = c.data();
    const size_t n = num_rows(a);

    for(size_t i = 0; i < n; ++i)
      for(size_t j = 0; j < n; ++j) {
        for(size_t k = 0; k < n; ++k)
          C[n*i + j] += A[n*i + k] * B[n*k + j];
      }
}

////////////////////////////////////////////////////////////////////////////////
// think about the access order

void gemm_ord(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){
    for(size_t i=0; i < num_rows(a); ++i) {
            for(size_t k=0; k < num_cols(a); ++k) {
        for(size_t j=0; j < num_cols(b); ++j) {
                c(i,j) += a(i,k) * b(k,j);
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// same as order, but caching values from A

void gemm_ord2(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){
    for(size_t i=0; i < num_rows(a); ++i) {
        for(size_t k=0; k < num_cols(a); ++k) {
            const double tmp = a(i,k);
            for(size_t j=0; j < num_cols(b); ++j) {
                c(i,j) += tmp * b(k,j);
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// same as order, but caching values from A (copying or transposing may help!)

void gemm_copy(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){
    const size_t n = num_rows(a);
    double * T = new double[n];

    for(size_t j = 0; j < n; ++j) {
      for(size_t l = 0; l < n; ++l)
        T[l] = b(l,j); // copy j-th B column
      for(size_t i = 0; i < n; ++i)
        for(size_t k = 0; k < n; ++k)
          c(i,j) += a(i,k) * T[k];
    }

    delete[] T;
}

////////////////////////////////////////////////////////////////////////////////
// use mtp for unrolling

template<int I>
inline double colexpand(const double * a, double * b, size_t offset) {
    //~ std::cout << "j:" << offset/128 << "  ";
    //~ std::cout << "(rec:" << I << ") ";
  return colexpand<I-1>(a, b, offset) +  a[offset + I]*b[I] ;  // left to right!
  //~ double x = colexpand<I-1>(a, b, offset) +  a[offset + I]*b[I] ;  // left to right!
  //~ std::cout << a[offset + I] << "*" << b[I] << " => " <<  x << " ";
  //~ return x;
}
template<>
inline double colexpand<0>(const double * a, double * b, size_t offset) {
  return a[offset+0]*b[0];
}
template<int LEN>
void gemm_exp(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){
    const size_t n = num_rows(a);
    double * T = new double[n];

    for(size_t j = 0; j < n; ++j) {
      for(size_t l = 0; l < n; ++l)
        T[l] = b(l,j); // copy j-th B column
      for(size_t i = 0; i < n; ++i)
        c(i,j) = colexpand<LEN-1>(a.data(), T, n*i);
    }

    delete[] T;
}

////////////////////////////////////////////////////////////////////////////////
// static single (scalar) assignment for ILP

void gemm_ssa(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){
    const double* A = a.data();
    const double* B = b.data();
    double* C = c.data();
    const size_t n = num_rows(a);

    for(size_t i = 0; i < n; ++i) {
        for(size_t j = 0; j < n; ++j) {
            const double* ap = A + i*n + j;
            const double* bp = B + j*n;
            double* cp = C + i*n;  // ptr to row
            const double* end = cp + n;
            while(cp < end) {
                *cp += (*ap) * (*bp);
                ++bp;
                ++cp;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// blocking the naive version

void gemm_block(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){
    const size_t X = 32;
    const size_t n = num_rows(a);
    for(size_t ib = 0; ib < n; ib += X)
      for(size_t jb = 0; jb < n; jb += X)
        for(size_t kb = 0; kb < n; kb += X)
          for(size_t i = ib; i < std::min(ib + X, n); ++i)
            for(size_t j = jb; j < std::min(jb + X, n); ++j)
              for(size_t k = kb; k < std::min(kb + X, n); ++k)
                c(i,j) += a(i,k) * b(k,j);
}

////////////////////////////////////////////////////////////////////////////////
// slightly smarter blocking

void gemm_block2(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){
    const size_t X = 64;
    const size_t n = num_rows(a);
    for(size_t ib = 0; ib < n; ib += X)
        for(size_t kb = 0; kb < n; kb += X)
      for(size_t jb = 0; jb < n; jb += X) {
          const size_t iend = std::min(ib+X, n);
          for(size_t i = ib; i < iend; ++i) {
              const size_t kend = std::min(kb + X, n);
              for(size_t k = kb; k < kend; ++k) {
              const double tmp = a(i,k);
            const size_t jend = std::min(jb + X, n);
            for(size_t j = jb; j < jend; ++j)
                c(i,j) += tmp * b(k,j);
              }
          }
        }
}

////////////////////////////////////////////////////////////////////////////////
// omp naive version

void gemm_omp(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){
    #pragma omp parallel for
    for(size_t i=0; i < num_rows(a); ++i) {
        for(size_t j=0; j < num_cols(b); ++j) {
            for(size_t k=0; k < num_cols(a); ++k) {
                c(i,j) += a(i,k) * b(k,j);
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// omp with better loop order, see Dense Linear Algebra lecture

void gemm_omp2(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){
    const size_t n = num_cols(a);
    #pragma omp parallel for
    for(size_t i=0; i < n; ++i) {
        for(size_t k=0; k < n; ++k) {
            const double tmp = a(i,k);
            for(size_t j=0; j < n; ++j)
                c(i,j) += tmp * b(k,j);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Strassen, starts being noticeable for 2000x2000 matrices

void gemm_blockStep(const double * A, const double * B, double * C, size_t n) {
  double tmp;
  size_t b = 32;

  for(size_t ib = 0; ib < n; ib += b)
    for(size_t jb = 0; jb < n; jb += b)
      for(size_t kb = 0; kb < n; kb += b)
        for(size_t i = ib; i < std::min(ib + b, n); ++i)
          for(size_t k = kb; k < std::min(kb + b, n); ++k) {
            tmp = A[n*i + k];
            for(size_t j = jb; j < std::min(jb + b, n); ++j)
              C[n*i + j] += tmp * B[n*k + j];
          }
}
void gemm_strass_impl(const double * A, const double * B, double * C, size_t n, size_t recurse = 1) {

  size_t m = n/2;
  size_t N = m*m;
  double * M1 = new double[N], * M2 = new double[N], * M3 = new double[N],
         * M4 = new double[N], * M5 = new double[N], * M6 = new double[N],
         * M7 = new double[N];
  std::fill_n(M1, N, 0); std::fill_n(M2, N, 0); std::fill_n(M3, N, 0); std::fill_n(M4, N, 0);
  std::fill_n(M5, N, 0); std::fill_n(M6, N, 0); std::fill_n(M7, N, 0);

  // M1 = (A11 + A22) * (B11 + B22)
  // M2 = (A21 + A22) * B11
  // M3 = A11 * (B12 - B22)
  // M4 = A22 * (B21 - B11)
  // M5 = (A11 + A12) * B22
  // M6 = (A21 - A11) * (B11 + B12)
  // M7 = (A12 - A22) * (B21 + B22)

  //~ for(size_t i = 0; i < m; ++i)                                                // trivial algorithm
    //~ for(size_t j = 0; j < m; ++j)
      //~ for(size_t k = 0; k < m; ++k) {
        //~ M1[m*i + j] += (A11 + A22) * (B11 + B22);
        //~ M2[m*i + j] += (A21 + A22) * B11;
        //~ M3[m*i + j] += A11 * (B12 - B22);
        //~ M4[m*i + j] += A22 * (B21 - B11);
        //~ M5[m*i + j] += (A11 + A12) * B22;
        //~ M6[m*i + j] += (A21 - A11) * (B11 + B12);
        //~ M7[m*i + j] += (A12 - A22) * (B21 + B22);
      //~ }


  //~ for(size_t i = 0; i < m; ++i)                                                // too slow unit step
    //~ for(size_t k = 0; k < m; ++k) {
      //~ double tmp1 = (A11 + A22), tmp2 = (A21 + A22), tmp3 = A11, tmp4 = A22,
             //~ tmp5 = (A11 + A12), tmp6 = (A21 - A11), tmp7 = (A12 - A22);
      //~ for(size_t j = 0; j < m; ++j) {
        //~ M1[m*i + j] += tmp1 * (B11 + B22);
        //~ M2[m*i + j] += tmp2 * B11;
        //~ M3[m*i + j] += tmp3 * (B12 - B22);
        //~ M4[m*i + j] += tmp4 * (B21 - B11);
        //~ M5[m*i + j] += tmp5 * B22;
        //~ M6[m*i + j] += tmp6 * (B11 + B12);
        //~ M7[m*i + j] += tmp7 * (B21 + B22);
      //~ }
    //~ }

  double * a1122 = new double[N], * a2122 = new double[N], * a11 = new double[N],
         * a22 = new double[N], * a1112 = new double[N], * a21m11 = new double[N],
         * a12m22 = new double[N],
         * b1122 = new double[N], * b11 = new double[N], * b12m22 = new double[N],
         * b21m11 = new double[N], * b22 = new double[N], * b1112 = new double[N],
         * b2122 = new double[N];

  for(size_t i = 0; i < m; ++i)
    for(size_t j = 0; j < m; ++j) {
      const size_t Z = m*i + j;
      a1122[Z] = A[n*i+j] + A[n*(i+m)+j+m];
      a2122[Z] = A[n*(i+m)+j] + A[n*(i+m)+j+m];
      a11[Z] = A[n*i+j];
      a22[Z] = A[n*(i+m)+j+m];
      a1112[Z] = A[n*i+j] + A[n*i+j+m];
      a21m11[Z] = A[n*(i+m)+j] - A[n*i+j];
      a12m22[Z] = A[n*i+j+m] - A[n*(i+m)+j+m];
      b1122[Z] = B[n*i+j] + B[n*(i+m)+j+m];
      b11[Z] = B[n*i+j];
      b12m22[Z] = B[n*i+j+m] - B[n*(i+m)+j+m];
      b21m11[Z] = B[n*(i+m)+j] - B[n*i+j];
      b22[Z] = B[n*(i+m)+j+m];
      b1112[Z] = B[n*i+j] + B[n*i+j+m];
      b2122[Z] =  B[n*(i+m)+j] + B[n*(i+m)+j+m];
    }

  if(recurse > 0) {

    gemm_strass_impl(a1122, b1122, M1, m, recurse - 1);
    gemm_strass_impl(a2122, b11, M2, m, recurse - 1);
    gemm_strass_impl(a11, b12m22, M3, m, recurse - 1);
    gemm_strass_impl(a22, b21m11, M4, m, recurse - 1);
    gemm_strass_impl(a1112, b22, M5, m, recurse - 1);
    gemm_strass_impl(a21m11, b1112, M6, m, recurse - 1);
    gemm_strass_impl(a12m22, b2122, M7, m, recurse - 1);

  } else {

  	// Use gemm_blockStep
    gemm_blockStep(a1122, b1122, M1, m);
    gemm_blockStep(a2122, b11, M2, m);
    gemm_blockStep(a11, b12m22, M3, m);
    gemm_blockStep(a22, b21m11, M4, m);
    gemm_blockStep(a1112, b22, M5, m);
    gemm_blockStep(a21m11, b1112, M6, m);
    gemm_blockStep(a12m22, b2122, M7, m);

  }

  delete[] a1122; delete[] a2122; delete[] a11; delete[] a22; delete[] a1112; delete[] a21m11; delete[] a12m22;
  delete[] b1122; delete[] b11; delete[] b12m22; delete[] b21m11; delete[] b22; delete[] b1112; delete[] b2122;

  // C11 = M1 + M4 - M5 + M7
  // C12 = M3 + M5
  // C21 = M2 + M4
  // C22 = M1 - M2 + M3 + M6

  for(size_t i = 0; i < m; ++i)
    for(size_t j = 0; j < m; ++j) {
      const size_t Z = m*i + j;
      C[n*i + j]       = M1[Z] + M4[Z] - M5[Z] + M7[Z];
      C[n*i + j+m]     = M3[Z] + M5[Z];
      C[n*(i+m) + j]   = M2[Z] + M4[Z];
      C[n*(i+m) + j+m] = M1[Z] - M2[Z] + M3[Z] + M6[Z];
    }

  delete[] M1; delete[] M2; delete[] M3; delete[] M4; delete[] M5; delete[] M6; delete[] M7;
}
void gemm_strass(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){
    gemm_strass_impl(a.data(), b.data(), c.data(), num_rows(a), 4);
}

////////////////////////////////////////////////////////////////////////////////
// vectorisation for just AVX2

void gemm_avx(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){
    const size_t n = num_rows(a);
    const double* A = a.data();
    double * T = new double[n];
    for(size_t j = 0; j < n; ++j) {
      for(size_t l = 0; l < n; ++l)
        T[l] = b(l,j); // copy j-th B column
      for(size_t i = 0; i < n; ++i) {
        __m256d sum = _mm256_setzero_pd();
        for(size_t k = 0; k < n; k+=4) {
          __m256d a = _mm256_load_pd(&A[n*i+k]);
          __m256d b = _mm256_load_pd(&T[k]);
          __m256d tmp = _mm256_mul_pd(a, b);
          sum = _mm256_add_pd(tmp, sum);
        }
        alignas(32) double s[4];
        _mm256_store_pd(&s[0], sum);
        c(i,j) += s[0] + s[1] + s[2] + s[3];
      }
    }
}

////////////////////////////////////////////////////////////////////////////////
// blas

void gemm_blas(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){
    const size_t n = num_rows(a);  // assuming same n for all (square) matrices
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
                n, n, n, 1.0, a.data(), n, b.data(), n, 1.0, c.data(), n);
}

////////////////////////////////////////////////////////////////////////////////
// combined: omp + avx
// note: tradeoff between pulling 4x btmp cachelines vs transposing the whole
//       B matrix (memory usage!)

void gemm_c1(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){
    const size_t n = num_rows(a);
    const double* A = a.data();

    #pragma omp parallel for
    for(size_t i=0; i < n; ++i) {
        for(size_t k=0; k < n; k+=4) {
            const __m256d a = _mm256_load_pd(&A[n*i+k]);
            for(size_t j=0; j < n; ++j) {
                const double btmp[] = {b(k+0,j),b(k+1,j),b(k+2,j),b(k+3,j)};
                const __m256d b = _mm256_load_pd(btmp);
                const __m256d tmp = _mm256_mul_pd(a, b);
                alignas(64) double s[4];
                _mm256_store_pd(&s[0], tmp);
                c(i,j) += s[0] + s[1] + s[2] + s[3];
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// blocking + omp
// note: for those observing slowdown when adding blocking to omp, touching
//       policy matters a lot, since threads shouldn't interfere!
//       see Linear Algebra lecture stripe plots (matrix*vector) for comparison

void gemm_c2(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){

    const size_t n = num_rows(a);
    const size_t X = 64;  // crucial but completely hardware-dependent!

    matrix_type::value_type* ctmp;
    #pragma omp parallel private(ctmp)
    {
        ctmp = new matrix_type::value_type[n*n];
        std::fill(ctmp,ctmp+n*n,0.);

        #pragma omp for
        for(size_t jb = 0; jb < n; jb += X)
        for(size_t kb = 0; kb < n; kb += X)
        for(size_t ib = 0; ib < n; ib += X) {
          const size_t iend = std::min(ib+X, n);
          for(size_t i = ib; i < iend; ++i) {
            const size_t kend = std::min(kb + X, n);
            for(size_t k = kb; k < kend; ++k) {
              const double tmp = a(i,k);
              const size_t jend = std::min(jb + X, n);
              for(size_t j = jb; j < jend; ++j) {
                ctmp[i*n+j] += tmp * b(k,j);
              }
            }
          }
        }

        #pragma omp critical
        {
            for(size_t i = 0; i < n; ++i)
            for(size_t j = 0; j < n; ++j) {
                c(i,j) += ctmp[i*n+j];
            }
        }

        delete[] ctmp;
    }
}

////////////////////////////////////////////////////////////////////////////////
// blocking + avx, with transposed matrix

void gemm_c3(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){

    const size_t n = num_rows(a);
    const size_t X = 64;  // crucial but completely hardware-dependent!

    matrix_type bt(n,n);
    for(size_t i = 0; i < n; ++i)
      for(size_t j = 0; j < n; ++j)
        bt(i,j) = b(j,i);

    for(size_t ib = 0; ib < n; ib += X)
    for(size_t kb = 0; kb < n; kb += X)
    for(size_t jb = 0; jb < n; jb += X) {
      const size_t iend = std::min(ib+X, n);
      for(size_t i = ib; i < iend; ++i) {
        const size_t kend = std::min(kb + X, n);
        for(size_t k = kb; k < kend; k+=4) {
          const size_t jend = std::min(jb + X, n);
          const __m256d av = _mm256_load_pd(&a(i,k));
          for(size_t j = jb; j < jend; ++j) {
            const __m256d bv = _mm256_load_pd(&bt(j,k));
            const __m256d tmp = _mm256_mul_pd(av, bv);
            alignas(64) double s[4];
            _mm256_store_pd(&s[0], tmp);
            c(i,j) += s[0] + s[1] + s[2] + s[3];
          }
        }
      }
    }

}

////////////////////////////////////////////////////////////////////////////////
// blocking + ssa

void gemm_c4(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){

    const size_t n = num_rows(a);
    const size_t X = 64;  // crucial but completely hardware-dependent!

    for(size_t ib = 0; ib < n; ib += X)
    for(size_t jb = 0; jb < n; jb += X)
    for(size_t kb = 0; kb < n; kb += X) {
      const size_t iend = std::min(ib+X, n);
      for(size_t i = ib; i < iend; ++i)
        for(size_t j = 0; j < X; ++j) {
          const double aval = a(i,j+kb);
          const double* bp = &b(kb+j,jb);
          double* cp = &c(i,jb);
          size_t blocksize = X+1;
          while(--blocksize) {
            *cp += aval * (*bp);
            ++bp;
            ++cp;
          }
        }
    }

/*
    // demo of ordering for the above block
    const size_t ndemo = 4;
    const size_t Xdemo = 2;
    for(size_t ib = 0; ib < ndemo; ib += Xdemo)
    for(size_t jb = 0; jb < ndemo; jb += Xdemo)
    for(size_t kb = 0; kb < ndemo; kb += Xdemo)
      for(size_t i = ib; i < std::min(ib + Xdemo, ndemo); ++i)
        for(size_t j = 0; j < Xdemo; ++j)
          for(size_t k = 0; k < Xdemo; ++k)
            std::cout << "a("<<i<<','<<kb+j<<") * "
                      << "b("<<kb+j<<','<<jb+k<<") = "
                      << "c("<<i<<','<<jb+k<<')'
                      << std::endl;
*/
}

////////////////////////////////////////////////////////////////////////////////
// blocking + ssa, with omp

void gemm_c5(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){


    const size_t n = num_rows(a);
    const size_t X = 64;

    #pragma omp parallel for
    for(size_t ib = 0; ib < n; ib += X)
    for(size_t jb = 0; jb < n; jb += X)
    for(size_t kb = 0; kb < n; kb += X) {
      const size_t iend = std::min(ib+X, n);
      for(size_t i = ib; i < iend; ++i)
        for(size_t j = 0; j < X; ++j) {
          const double aval = a(i,j+kb);
          const double* bp = &b(kb+j,jb);
          double* cp = &c(i,jb);
          size_t blocksize = X+1;
          while(--blocksize) {
            *cp += aval * (*bp);
            ++bp;
            ++cp;
          }
        }
    }

}

////////////////////////////////////////////////////////////////////////////////
// blocking + ssa + omp + avx, loop order

void gemm_c6(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){

    const size_t n = num_rows(a);
    const size_t X = 64;
    alignas(64) double s[4];

    for(size_t ib = 0; ib < n; ib += X)
    for(size_t kb = 0; kb < n; kb += X)
    for(size_t jb = 0; jb < n; jb += X) {
      const size_t iend = std::min(ib+X, n);
      #pragma omp parallel for private(s)
      for(size_t i = ib; i < iend; ++i)
        for(size_t j = 0; j < X; ++j) {
          const __m256d av = _mm256_set1_pd(a(i,j+kb));
          const double* bp = &b(kb+j,jb);
          double* cp = &c(i,jb);
          size_t blocksize = X+4;
          while(blocksize -= 4) {  // landmine: blocksize must divide 4 !
            const __m256d bv = _mm256_load_pd(bp);
            const __m256d tmp = _mm256_mul_pd(av, bv);
            _mm256_store_pd(&s[0], tmp);
            cp[0] += s[0];
            cp[1] += s[1];
            cp[2] += s[2];
            cp[3] += s[3];
            bp += 4;
            cp += 4;
          }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// unrolled c6, slowness indicates bad ordering in memory access (improvable!)

void gemm_c6u(
      matrix_type const& a
    , matrix_type const& b
    , matrix_type& c
    ){


    const size_t n = num_rows(a);
    const size_t X = 64;
    alignas(64) double s[64];

    #pragma omp parallel for private(s)
    for(size_t ib = 0; ib < n; ib += X)
    for(size_t kb = 0; kb < n; kb += X)
    for(size_t jb = 0; jb < n; jb += X) {
      const size_t iend = std::min(ib+X, n);
      for(size_t i = ib; i < iend; ++i)
        for(size_t j = 0; j < X; ++j) {

          // for i in $(seq 0 4 60); do echo foo $i bar; done
          // alternatively use Cheetah

          const __m256d av = _mm256_set1_pd(a(i,j+kb));

          const __m256d bv00 = _mm256_load_pd(&b(kb+j,jb+ 0));
          const __m256d bv04 = _mm256_load_pd(&b(kb+j,jb+ 4));
          const __m256d bv08 = _mm256_load_pd(&b(kb+j,jb+ 8));
          const __m256d bv12 = _mm256_load_pd(&b(kb+j,jb+12));
          const __m256d bv16 = _mm256_load_pd(&b(kb+j,jb+16));
          const __m256d bv20 = _mm256_load_pd(&b(kb+j,jb+20));
          const __m256d bv24 = _mm256_load_pd(&b(kb+j,jb+24));
          const __m256d bv28 = _mm256_load_pd(&b(kb+j,jb+28));
          const __m256d bv32 = _mm256_load_pd(&b(kb+j,jb+32));
          const __m256d bv36 = _mm256_load_pd(&b(kb+j,jb+36));
          const __m256d bv40 = _mm256_load_pd(&b(kb+j,jb+40));
          const __m256d bv44 = _mm256_load_pd(&b(kb+j,jb+44));
          const __m256d bv48 = _mm256_load_pd(&b(kb+j,jb+48));
          const __m256d bv52 = _mm256_load_pd(&b(kb+j,jb+52));
          const __m256d bv56 = _mm256_load_pd(&b(kb+j,jb+56));
          const __m256d bv60 = _mm256_load_pd(&b(kb+j,jb+60));

          const __m256d tmp00 = _mm256_mul_pd(av, bv00);
          const __m256d tmp04 = _mm256_mul_pd(av, bv04);
          const __m256d tmp08 = _mm256_mul_pd(av, bv08);
          const __m256d tmp12 = _mm256_mul_pd(av, bv12);
          const __m256d tmp16 = _mm256_mul_pd(av, bv16);
          const __m256d tmp20 = _mm256_mul_pd(av, bv20);
          const __m256d tmp24 = _mm256_mul_pd(av, bv24);
          const __m256d tmp28 = _mm256_mul_pd(av, bv28);
          const __m256d tmp32 = _mm256_mul_pd(av, bv32);
          const __m256d tmp36 = _mm256_mul_pd(av, bv36);
          const __m256d tmp40 = _mm256_mul_pd(av, bv40);
          const __m256d tmp44 = _mm256_mul_pd(av, bv44);
          const __m256d tmp48 = _mm256_mul_pd(av, bv48);
          const __m256d tmp52 = _mm256_mul_pd(av, bv52);
          const __m256d tmp56 = _mm256_mul_pd(av, bv56);
          const __m256d tmp60 = _mm256_mul_pd(av, bv60);

          _mm256_store_pd(&s[ 0], tmp00);
          _mm256_store_pd(&s[ 4], tmp04);
          _mm256_store_pd(&s[ 8], tmp08);
          _mm256_store_pd(&s[12], tmp12);
          _mm256_store_pd(&s[16], tmp16);
          _mm256_store_pd(&s[20], tmp20);
          _mm256_store_pd(&s[24], tmp24);
          _mm256_store_pd(&s[28], tmp28);
          _mm256_store_pd(&s[32], tmp32);
          _mm256_store_pd(&s[36], tmp36);
          _mm256_store_pd(&s[40], tmp40);
          _mm256_store_pd(&s[44], tmp44);
          _mm256_store_pd(&s[48], tmp48);
          _mm256_store_pd(&s[52], tmp52);
          _mm256_store_pd(&s[56], tmp56);
          _mm256_store_pd(&s[60], tmp60);

          c(i,jb+ 0) += s[ 0];
          c(i,jb+ 1) += s[ 1];
          c(i,jb+ 2) += s[ 2];
          c(i,jb+ 3) += s[ 3];
          c(i,jb+ 4) += s[ 4];
          c(i,jb+ 5) += s[ 5];
          c(i,jb+ 6) += s[ 6];
          c(i,jb+ 7) += s[ 7];
          c(i,jb+ 8) += s[ 8];
          c(i,jb+ 9) += s[ 9];
          c(i,jb+10) += s[10];
          c(i,jb+11) += s[11];
          c(i,jb+12) += s[12];
          c(i,jb+13) += s[13];
          c(i,jb+14) += s[14];
          c(i,jb+15) += s[15];
          c(i,jb+16) += s[16];
          c(i,jb+17) += s[17];
          c(i,jb+18) += s[18];
          c(i,jb+19) += s[19];
          c(i,jb+20) += s[20];
          c(i,jb+21) += s[21];
          c(i,jb+22) += s[22];
          c(i,jb+23) += s[23];
          c(i,jb+24) += s[24];
          c(i,jb+25) += s[25];
          c(i,jb+26) += s[26];
          c(i,jb+27) += s[27];
          c(i,jb+28) += s[28];
          c(i,jb+29) += s[29];
          c(i,jb+30) += s[30];
          c(i,jb+31) += s[31];
          c(i,jb+32) += s[32];
          c(i,jb+33) += s[33];
          c(i,jb+34) += s[34];
          c(i,jb+35) += s[35];
          c(i,jb+36) += s[36];
          c(i,jb+37) += s[37];
          c(i,jb+38) += s[38];
          c(i,jb+39) += s[39];
          c(i,jb+40) += s[40];
          c(i,jb+41) += s[41];
          c(i,jb+42) += s[42];
          c(i,jb+43) += s[43];
          c(i,jb+44) += s[44];
          c(i,jb+45) += s[45];
          c(i,jb+46) += s[46];
          c(i,jb+47) += s[47];
          c(i,jb+48) += s[48];
          c(i,jb+49) += s[49];
          c(i,jb+50) += s[50];
          c(i,jb+51) += s[51];
          c(i,jb+52) += s[52];
          c(i,jb+53) += s[53];
          c(i,jb+54) += s[54];
          c(i,jb+55) += s[55];
          c(i,jb+56) += s[56];
          c(i,jb+57) += s[57];
          c(i,jb+58) += s[58];
          c(i,jb+59) += s[59];
          c(i,jb+60) += s[60];
          c(i,jb+61) += s[61];
          c(i,jb+62) += s[62];
          c(i,jb+63) += s[63];

        }
    }
}

////////////////////////////////////////////////////////////////////////////////

int main() {


    const int n = 1536;
    //~ const int n = 512+256;  // below default 900 recursion depth
    //~ const int n = 64;  // check small matrices too (with high repeats)

    const int repeats = 2.*1e10/n/n/n;  // some reasonable workload
    std::cout << "Repeats: " << repeats << std::endl << std::endl;

    matrix_type b(n,n);
    matrix_type a(n,n);

    // Fill matrices a and b with some values
    double x = 0.0;
    std::generate_n( a.data(), num_rows(a)*num_cols(a), [&x]() -> double { x+=0.1; return x; });
    std::generate_n( b.data(), num_rows(b)*num_cols(b), [&x]() -> double { x-=0.15; return x; });
    //~ print_matrix(a, 6);
    //~ print_matrix(b, 6);

    // for diagnostics
    //~ for(size_t i = 0; i < n; ++i)
    //~ for(size_t j = 0; j < n; ++j) {
        //~ static int x = 0;
        //~ a(i,j) = x;
        //~ b(i,j) = x;
        //~ ++x;
    //~ }

    const std::map<std::string, decltype(std::function<decltype(gemm)>())> algos = {
          std::make_pair("gemm_blas", gemm_blas)
        //~ , std::make_pair("gemm", gemm)
        //~ , std::make_pair("gemm_array", gemm_array)
        //~ , std::make_pair("gemm_ord", gemm_ord)
        //~ , std::make_pair("gemm_ord2", gemm_ord2)
        //~ , std::make_pair("gemm_copy", gemm_copy)
        //~ , std::make_pair("gemm_exp<n>", gemm_exp<n>)
        //~ , std::make_pair("gemm_ssa", gemm_ssa)
        //~ , std::make_pair("gemm_block", gemm_block)
        //~ , std::make_pair("gemm_block2", gemm_block2)
        //~ , std::make_pair("gemm_omp", gemm_omp)
        //~ , std::make_pair("gemm_omp2", gemm_omp2)
        //~ , std::make_pair("gemm_strass", gemm_strass)
        //~ , std::make_pair("gemm_avx", gemm_avx)
        //~ , std::make_pair("gemm_c1", gemm_c1)  // omp+avx
        //~ , std::make_pair("gemm_c2", gemm_c2)  // blocking+omp
        //~ , std::make_pair("gemm_c3", gemm_c3)  // blocking+avx
        //~ , std::make_pair("gemm_c4", gemm_c4)  // blocking+ssa
        , std::make_pair("gemm_c5", gemm_c5)  // blocking+ssa with simple omp
        , std::make_pair("gemm_c6", gemm_c6)  // blocking+ssa+omp+avx
        //~ , std::make_pair("gemm_c6u", gemm_c6u)  // "helping" the compiler
    };

    matrix_type ref(n,n);
    gemm_blas(a, b, ref);  // reference
    //~ print_matrix(ref, 6);
    for(auto& alg: algos) {
        std::cout << "checking " << alg.first << std::endl;
        check(alg.second, a, b, ref, 1e-6);
    }

    std::cout << std::endl;
    std::map<std::string, std::pair<double,double> > results;
    for(auto& alg: algos) {
        matrix_type c(n,n);
        std::cout << "benchmarking " << alg.first << std::endl;
        results[alg.first] = benchmark(alg.second, a, b, c, repeats);
        //~ print_matrix(c, 6);
    }

    std::cout << std::endl;
    for(auto& res: results) {
        std::cout << res.first <<  ":" << std::setw(5) << "\t" << res.second.first
                  << "s  \t+- " << res.second.second << "s     \tblas x"
                  << res.second.first / results["gemm_blas"].first
                  << std::endl;
    }

    return 0;

}
