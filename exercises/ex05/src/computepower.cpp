/*
 *  computepower.cpp
 *  Copyright 2016 ETH Zurich. All rights reserved.
 *
 */

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <map>
#include <vector>
#include <string>
#include <cassert>

#include <algorithm>
#include <sys/time.h>

#if defined(_OPENMP)
#include <omp.h>
#endif

#include "ArgumentParser.hpp"

#define MULADD(b, x, a) (b*(x+a))
#define SUM8(x0, x1, x2, x3, x4, x5, x6, x7) (((x0 + x1) + (x2 + x3)) + ((x4 + x5) + (x6 + x7)))


//========================================================================================
//
//	Helper Functions
//
//========================================================================================

// timer method
double mysecond() {
#if defined(_OPENMP)
	return omp_get_wtime();
#else
	struct timeval tp;
	struct timezone tzp;
	int i;

	i = gettimeofday(&tp,&tzp);
	return ( (double) tp.tv_sec + (double) tp.tv_usec * 1.e-6 );
#endif
}


void ComputePower(double * s, int iSize) {
	const int static N = iSize; {
		// store coefficients in registers
		const double a0 = 0.01f;
		const double a1 = 0.035f;
		const double a2 = 0.001f;
		const double a3 = 0.15f;

		const double b0 = 0.012f;
		const double b1 = 0.067f;
		const double b2 = 0.02f;
		const double b3 = 0.21f;


		// 8 independent streams of computations
		// this helps filling the processing pipeline
		// 8 data reads
		double x0 = s[0];
		double y0 = s[0];
		double z0 = s[0];
		double w0 = s[0];
		double r0 = s[0];
		double t0 = s[0];
		double u0 = s[0];
		double v0 = s[0];

		// compute independently 8 polynomial evaluations with horner's scheme of degree 4N
		// 4*8*2*N FLOP
		for (int i=0; i<N; i++) {
			const double gx0 = MULADD(b0, x0, a0);
			const double gy0 = MULADD(b0, y0, a0);
			const double gz0 = MULADD(b0, z0, a0);
			const double gw0 = MULADD(b0, w0, a0);
			const double gr0 = MULADD(b0, r0, a0);
			const double gt0 = MULADD(b0, t0, a0);
			const double gu0 = MULADD(b0, u0, a0);
			const double gv0 = MULADD(b0, v0, a0);

			const double tx0 = MULADD(b1, gx0, a1);
			const double ty0 = MULADD(b1, gy0, a1);
			const double tz0 = MULADD(b1, gz0, a1);
			const double tw0 = MULADD(b1, gw0, a1);
			const double tr0 = MULADD(b1, gr0, a1);
			const double tt0 = MULADD(b1, gt0, a1);
			const double tu0 = MULADD(b1, gu0, a1);
			const double tv0 = MULADD(b1, gv0, a1);

			const double ux0 = MULADD(b2, tx0, a2);
			const double uy0 = MULADD(b2, ty0, a2);
			const double uz0 = MULADD(b2, tz0, a2);
			const double uw0 = MULADD(b2, tw0, a2);
			const double ur0 = MULADD(b2, tr0, a2);
			const double ut0 = MULADD(b2, tt0, a2);
			const double uu0 = MULADD(b2, tu0, a2);
			const double uv0 = MULADD(b2, tv0, a2);

			x0 = MULADD(b3, ux0, a3);
			y0 = MULADD(b3, uy0, a3);
			z0 = MULADD(b3, uz0, a3);
			w0 = MULADD(b3, uw0, a3);
			r0 = MULADD(b3, ur0, a3);
			t0 = MULADD(b3, ut0, a3);
			u0 = MULADD(b3, uu0, a3);
			v0 = MULADD(b3, uv0, a3);
		}

		// sum the results of the 8 polynomial evaluations
		// and store in the output array
		// without this step, the compiler might simplify the code by discarding unused computation and data
		// 1 data write, 7 FLOP (irrelevant if N is large enough)
		s[0] = SUM8(x0, y0, z0, w0, r0, t0, u0, v0);
	}
}

//========================================================================================
//
//	main
//
//========================================================================================

using namespace std;

int main(int argc, const char * argv[]) {
	ArgumentParser arg(argc,argv);

	// get the number of threads
	int nthreads = 0;
	#pragma omp parallel
	{
		#pragma omp atomic
		nthreads += 1;
	}

	// getting parameters
	int iSize = arg.get("size", 1e7);
	int iIteration = arg.get("iterations", 10);

	// storage
	double * timeHOI = new double[iIteration];
	map<string, vector<double> > peakPerformance;
	double * s = new double;

	// initialize value for the polynomial evaluation
	s[0] = 1e-6;

	// run the benchmark iIteration times
	for(int i=0; i<iIteration; i++) {
		timeHOI[i] = mysecond();
		#pragma omp parallel
		{
			ComputePower(s,iSize);
		}
		timeHOI[i] = mysecond() - timeHOI[i];
	}

	// compute performance of each benchmark run
	for(int i=0; i<iIteration; i++)
		peakPerformance["HOI"].push_back(1.e-9*(double)iSize*4*2*8*nthreads / timeHOI[i]);

	sort(peakPerformance["HOI"].begin(), peakPerformance["HOI"].end(), std::greater<double>());

	// percentiles to be selected
	const int I1 = iIteration*.9;
	const int I2 = iIteration*.5;
	const int I3 = iIteration*.1;

	// output 10th, 50th, 90th percentiles
	cout << "Ranked  Peak Performance" << endl
	     << I3 << "\t" << peakPerformance["HOI"][I3] << "GFLOP/s" << endl
	     << I2 << "\t" << peakPerformance["HOI"][I2] << "GFLOP/s" << endl
	     << I1 << "\t" << peakPerformance["HOI"][I1] << "GFLOP/s" << endl;

	return 0;
}
