/*******************************************************************************
 * 
 * Argument parser v1.2
 * (C) 2012-2014 Donjan Rodic <donjan@dyx.ch>
 * Licensed under the WTFPL (http://sam.zoy.org/wtfpl/)
 * 
 * Simple argument parser that fetches command line arguments without previous
 * declaration, assuming the programmer plans to have defaults for all named
 * arguments.
 * 
 * 
 * REFERENCE
 * 
 * class ArgumentParser {
 *   ArgumentParser(int, char **);
 *   T      get(char, T);
 *   T      get(std::string, T);
 *   T      get(unsigned int);
 *   size_t argcFree();
 *   class RangeError : public std::runtime_error;
 * };
 * 
 * 
 * USAGE
 * 
 *   ArgumentParser arg(argc, argv);                     // early in main
 *   int answer = arg.get('d', 42);                      // named argument
 *   char gender = arg.get("gender", 'm');               // named argument
 *   std::string targetfile = arg.get(1);                // "free" argument
 *   try{ arg.get(999); } catch(ArgumentParser::RangeError e) {}
 *   int freeCount = arg.argcFree();
 * 
 * The first argument of get() can be a char, string literal or std::string.
 * The second argument defaults the value if not given on the command line.
 * "Free" (unnamed) arguments are retrieved with the single-argument get().
 * For type casting, check the NOTES ON TYPE CONVERSION below.
 * The zero element get(0) is the name of the executable.
 * Upon out-of-bounds accessing free arguments, an ArgumentParser::RangeError
 * will be thrown.
 * The number of free arguments accessible is given by the argcFree() method and
 * does not count the zero element: get(argcFree()) is always successful.
 * 
 * 
 * COMMAND LINE BEHAVIOUR
 * 
 * Both single-dash (-o) and double-dash (--out) syntax is supported.
 * Double-dash arguments like --out are stored as "out".
 * Single-dash arguments are always 1 character wide: -out is stored as 'o' with
 * parameter value "ut", enabling the common UNIX CLI syntax -n3 (besides -n 3).
 * Arguments followed by either a dash-parameter or nothing (example: -a -b) are
 * assumed to be flags and set to "1" or true (in this case both 'a' and 'b').
 * 
 * Negative values can be passed as -n-56, but not -n -56, because the latter
 * assumes that -n is a flag and -56 sets the argument '5' to the value "6".
 * Currently no negative parameters can be passed to double-dash arguments.
 * On duplicate arguments, ArgumentParser will issue a warning to stdout and
 * save only the newer (further right) one.
 * 
 * 
 * NOTES ON TYPE CONVERSION
 * 
 * The return type of get() is defaulted to the type of the second parameter.
 * The optional template argument can be omitted if the user is confident that
 * automatic type casting is appropriate (which it generally is).
 * For example if no argument -d via command line is given:
 * 
 *   int    res = arg.get        ("d", 9.7);       // res == 9
 *   int    res = arg.get<int>   ("d", 9.7);       // res == 9
 *   int    res = arg.get<double>("d", 9.7);       // res == 9
 *   double res = arg.get        ("d", 9.7);       // res == 9.7
 *   double res = arg.get<int>   ("d", 9.7);       // res == 9.0
 *   double res = arg.get<double>("d", 9.7);       // res == 9.7
 * 
 * A special case are second arguments of type char* and const char*:
 * 
 *   arg.get('f', "default.out");
 * 
 * and
 * 
 *   char defout[] = "default.out";
 *   arg.get('f', defout);
 * 
 * are both internally converted to
 * 
 *   arg.get<std::string>('f', std::string("default.out"));
 * 
 * unless specifially called with a <char*> or <const char*> template parameter.
 * Likewise, all unnamed/free arguments are treated as std::string unless called
 * with a template parameter:
 * 
 *   std::string str = arg.get(1);          // str receives a std::string
 *   std::string str = arg.get<int>(1);     // error
 *   int val = arg.get(1);                  // error
 *   int val = arg.get<int>(1);             // val receives an integer
 * 
 ******************************************************************************/

#pragma once

#include <iostream>
#include <sstream>
#include <cstring>
#include <map>
#include <vector>
#include <stdexcept>


class ArgumentParser {

  private:

  std::map<std::string, std::string> arguments;
  std::vector<std::string> freeArguments;
  size_t _argcFree;

  template<typename T> struct fwdtype { };  // simulate method specialisation


  public:

  class RangeError : public std::runtime_error {
    public:
    template<typename T>
    RangeError(T param) : std::runtime_error(param) {}
  };

  ArgumentParser(int argc, const char ** argv) : _argcFree(0) {

    for(int i = 0; i < argc; ++i) {

      std::string str(argv[i]),
                  argname,
                  argval = std::string(1,i);

      if(str[0] == '-') {

        if(str[1] == '-' )                           // --arg
          argname = str.substr(2, str.size() -2);
        else                                         // -a
          argname = str[1];

        if(str[1] != '-' && str.size() >= 3)         // we have -n30
          argval = str.substr(2, str.size()-2);
        else if( i < argc-1 && argv[i+1][0] != '-' ) // there is a parameter
          argval = argv[++i];                        // assign and skip one i
        else                                         // it's a flag
          argval = "1";                              // set it to "true"

        check(argname) = argval;

      } else {                                       // free string

        freeArguments.push_back(str);
        if(i != 0) ++_argcFree;

      }
    }

    // Diagnostics
    //~ std::cout << "\nNamed arguments:";
    //~ for(std::map<std::string, std::string>::iterator it = arguments.begin();
        //~ it != arguments.end(); ++it)
      //~ std::cout << "\n" << it->first << " : " << it->second;
    //~ std::cout << "\nFree floating arguments:";
    //~ for(unsigned int k = 0; k < freeArguments.size(); ++k)
      //~ std::cout << "\n" << k << " : " << freeArguments[k];
    //~ std::cout << "\n";

  }

  size_t argcFree() { return _argcFree; }


  private:

  // Check if argument is already present and print a warning
  std::string & check(const std::string & s) {
    if(arguments.find(s) != arguments.end())
      std::cout << "\nArgumentParser duplicate warning for argument: "
                << s << std::endl;
    return arguments[s];
  }
  std::string & check(const char & c) {
    return check(std::string(1,c));
  }


  public:

  // Simulate template specialisation with overloaded implementation methods
  template <typename T>
  T get(const std::string & key, const T dfault) {
    return get_impl(key, dfault, fwdtype<T>());
  }

  template <typename T>
  T get(const char key, const T dfault) {
    return get(std::string(1, key), dfault);
  }

  template <typename T>
  T get(const unsigned int k) {
    return get_impl(k, fwdtype<T>());
  }

  // Shortcuts for handling constant character strings as default parameters
  std::string get(const std::string & key, const char * dfault) {
    return get(key, std::string(dfault));
  }
  std::string get(const std::string & key, char * dfault) {
    return get(key, std::string(dfault));
  }
  std::string get(const char key, const char * dfault) {
    return get(key, std::string(dfault));
  }
  std::string get(const char key, char * dfault) {
    return get(key, std::string(dfault));
  }
  std::string get(const unsigned int k) {
    return get<std::string>(k);
  }


  private:

  // Implementation for named arguments
  template <typename T>
  T get_impl(const std::string & key, const T & dfault, fwdtype<T>) {
    std::map<std::string, std::string>::iterator found = arguments.find(key);
    if(found != arguments.end()) {
      T ret;
      std::stringstream ss(arguments[key]);
      ss >> ret;
      return ret;
    } else
      return dfault;
  }

  // Method "specialisation", needed to preserve strings because stringstream
  // stops at whitespaces ("foo bar" gets split up)
  std::string get_impl(const std::string & key, const std::string & dfault,
                       fwdtype<std::string>) {
    std::map<std::string, std::string>::iterator found = arguments.find(key);
    if(found != arguments.end())
      return arguments[key];
    else
      return dfault;
  }

  // Implementation for free arguments
  template <typename T>
  T get_impl(const unsigned int k, fwdtype<T>) {
    if(k >= freeArguments.size())
      throw RangeError("Out of bounds access.");

    std::stringstream ss(freeArguments[k]);
    T ret;
    ss >> ret;
    return ret;
  }

  // Method "specialisation", needed to preserve strings because stringstream
  // stops at whitespaces ("foo bar" gets split up)
  template <typename T>
  T get_impl(const unsigned int k, fwdtype<std::string>) {
    if(k >= freeArguments.size())
      throw RangeError("Out of bounds access.");

    return freeArguments[k];
  }

};
