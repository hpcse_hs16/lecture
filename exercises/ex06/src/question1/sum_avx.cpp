#include <vector>
#include <numeric>
#include <iostream>
#include <x86intrin.h>
#include "timer.hpp"
#include "aligned_allocator.hpp"


int main( int argc, char** argv )
{
    // repetitions
    const int nrep = 10000;
    // vector size
    const int n = 1<<18;

    // initialize 64 byte aligned vectors
    std::vector< float, hpcse::aligned_allocator<float,64> > x(n,-1.2), y(n,3.4), z(n);

    hpcse::timer<> tim;
    tim.start();
    for (int k = 0; k < nrep; ++k)
    {
        const __m256 f2 = _mm256_set1_ps( 2.f );
        for( int i = 0; i < n; i += 8 )
        {
            // z[i] = x[i]*x[i] + y[i]*y[i] + 2.*x[i]*y[i];
            const __m256 xx = _mm256_load_ps( &x[i] );
            const __m256 yy = _mm256_load_ps( &y[i] );

            const __m256 z1 = _mm256_mul_ps( xx, xx );
            const __m256 z2 = _mm256_mul_ps( yy, yy );
            const __m256 z3 = _mm256_mul_ps( xx, yy );
            const __m256 z4 = _mm256_mul_ps( f2, z3 );

            const __m256 zz = _mm256_add_ps( z1, z2 );
            const __m256 res = _mm256_add_ps( zz, z4 );

            // Store back the result.
            // Since the result won't be used any time soon it is useless to
            // store it in the caches. It would only pollute the caches. Hence
            // we could store it back to the RAM bypassing the cache-hierarchy by
            // using _mm256_stream_ps instead of the regular _mm256_store_ps.
            // However, _mm256_store_ps is faster for such small sizes.
            _mm256_store_ps( &z[i], res );
        }
    }
    tim.stop();

    // print result checksum
    std::cout << std::accumulate(z.begin(), z.end(), 0.) << std::endl;
    std::cout << "Task took " << tim.get_timing() << " seconds." << std::endl;
}

