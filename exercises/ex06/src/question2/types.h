/* File:   types.h */
/* Copyright 2015 ETH Zurich. All Rights Reserved. */
#ifndef TYPES_H_FGO79RJB
#define TYPES_H_FGO79RJB

#include <cstddef>
#include <utility>
#include <vector>

#if defined(_AVX256_)
typedef float value_type;
#elif defined(_ANY_)
#ifndef REG
#define REG 1
#endif /* REG */

#if REG == 0
#include "vector_types/m256d.hpp"
typedef double value_type;
typedef m256d register_type;
#elif REG == 1
#include "vector_types/m256f.hpp"
typedef float value_type;
typedef m256f register_type;
#elif REG == 2
#include "vector_types/m128d.hpp"
typedef double value_type;
typedef m128d register_type;
#elif REG == 3
#include "vector_types/m128f.hpp"
typedef float value_type;
typedef m128f register_type;
#endif

#else
#ifdef _SINGLE_PRECISION_
typedef float value_type;
#else
typedef double value_type;
#endif /* _SINGLE_PRECISION_ */
#endif

// to ensure 64-Byte alignment for AVX 256-bit registers
#include "aligned_allocator.hpp"

typedef std::size_t size_type;

typedef std::pair<std::vector<value_type, hpc15::aligned_allocator<value_type,64> >, std::vector<value_type, hpc15::aligned_allocator<value_type,64> > > configuration_type;
// first = x, second = y

#endif /* TYPES_H_FGO79RJB */
