#ifndef M128d_HPP
#define M128d_HPP

#include <xmmintrin.h>

struct m128d
{
  m128d()
    : val(_mm_setzero_pd())
  {}

  m128d(const double val_)
    : val(_mm_set1_pd(val_))
  {}

  m128d(const double* val_)
    : val(_mm_load_pd(val_))
  {}

  unsigned get_register_width() const {return r_width;}

  double sum() const
  {
    double sums[r_width];
    _mm_store_pd(sums,val);
    return std::accumulate(sums,sums+r_width,0.0);
  }

  m128d& operator-=(const m128d& a)
  {
    val = _mm_sub_pd(this->val,a.val);
    return *this;
  }

  m128d& operator+=(const m128d& a)
  {
    val = _mm_add_pd(this->val,a.val);
    return *this;
  }

  m128d& operator*=(const m128d& a)
  {
    val = _mm_mul_pd(this->val,a.val);
    return *this;
  }

  m128d& operator/=(const m128d& a)
  {
    val = _mm_div_pd(this->val,a.val);
    return *this;
  }

  __m128d val;
  static const unsigned r_width = 2;
};

inline m128d operator-(m128d a, const m128d& b)
{
  a -= b;
  return a;
}

inline m128d operator+(m128d a, const m128d& b)
{
  a += b;
  return a;
}

inline m128d operator*(m128d a, const m128d& b)
{
  a *= b;
  return a;
}

inline m128d operator/(m128d a, const m128d& b)
{
  a /= b;
  return a;
}

#endif
