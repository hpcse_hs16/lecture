#ifndef M256f_HPP
#define M256f_HPP

#include <immintrin.h>

struct m256f
{
  m256f()
    : val(_mm256_setzero_ps())
  {}

  m256f(const float val_)
    : val(_mm256_set1_ps(val_))
  {}

  m256f(const float* val_)
    : val(_mm256_load_ps(val_))
  {}

  unsigned get_register_width() const {return r_width;}

  float sum() const
  {
    float sums[r_width];
    _mm256_store_ps(sums,val);
    return std::accumulate(sums,sums+r_width,0.0);
  }

  m256f& operator-=(const m256f& a)
  {
    val = _mm256_sub_ps(this->val,a.val);
    return *this;
  }

  m256f& operator+=(const m256f& a)
  {
    val = _mm256_add_ps(this->val,a.val);
    return *this;
  }

  m256f& operator*=(const m256f& a)
  {
    val = _mm256_mul_ps(this->val,a.val);
    return *this;
  }

  m256f& operator/=(const m256f& a)
  {
    val = _mm256_div_ps(this->val,a.val);
    return *this;
  }

  __m256 val;
  static const unsigned r_width = 8;
};

inline m256f operator-(m256f a, const m256f& b)
{
  a -= b;
  return a;
}

inline m256f operator+(m256f a, const m256f& b)
{
  a += b;
  return a;
}

inline m256f operator*(m256f a, const m256f& b)
{
  a *= b;
  return a;
}

inline m256f operator/(m256f a, const m256f& b)
{
  a /= b;
  return a;
}

#endif
