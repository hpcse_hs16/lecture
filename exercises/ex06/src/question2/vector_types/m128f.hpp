#ifndef M128f_HPP
#define M128f_HPP

#include <xmmintrin.h>

struct m128f
{
  m128f()
    : val(_mm_setzero_ps())
  {}

  m128f(const float val_)
    : val(_mm_set1_ps(val_))
  {}

  m128f(const float* val_)
    : val(_mm_load_ps(val_))
  {}

  unsigned get_register_width() const {return r_width;}

  float sum() const
  {
    float sums[r_width];
    _mm_store_ps(sums,val);
    return std::accumulate(sums,sums+r_width,0.0);
  }

  m128f& operator-=(const m128f& a)
  {
    val = _mm_sub_ps(this->val,a.val);
    return *this;
  }

  m128f& operator+=(const m128f& a)
  {
    val = _mm_add_ps(this->val,a.val);
    return *this;
  }

  m128f& operator*=(const m128f& a)
  {
    val = _mm_mul_ps(this->val,a.val);
    return *this;
  }

  m128f& operator/=(const m128f& a)
  {
    val = _mm_div_ps(this->val,a.val);
    return *this;
  }

  __m128 val;
  static const unsigned r_width = 4;
};

inline m128f operator-(m128f a, const m128f& b)
{
  a -= b;
  return a;
}

inline m128f operator+(m128f a, const m128f& b)
{
  a += b;
  return a;
}

inline m128f operator*(m128f a, const m128f& b)
{
  a *= b;
  return a;
}

inline m128f operator/(m128f a, const m128f& b)
{
  a /= b;
  return a;
}

#endif
