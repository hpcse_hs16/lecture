#ifndef M256d_HPP
#define M256d_HPP

#include <immintrin.h>

struct m256d
{
  m256d()
    : val(_mm256_setzero_pd())
  {}

  m256d(const double val_)
    : val(_mm256_set1_pd(val_))
  {}

  m256d(const double* val_)
    : val(_mm256_load_pd(val_))
  {}

  unsigned get_register_width() const {return r_width;}

  double sum() const
  {
    double sums[r_width];
    _mm256_store_pd(sums,val);
    return std::accumulate(sums,sums+r_width,0.0);
  }

  m256d& operator-=(const m256d& a)
  {
    val = _mm256_sub_pd(this->val,a.val);
    return *this;
  }

  m256d& operator+=(const m256d& a)
  {
    val = _mm256_add_pd(this->val,a.val);
    return *this;
  }

  m256d& operator*=(const m256d& a)
  {
    val = _mm256_mul_pd(this->val,a.val);
    return *this;
  }

  m256d& operator/=(const m256d& a)
  {
    val = _mm256_div_pd(this->val,a.val);
    return *this;
  }

  __m256d val;
  static const unsigned r_width = 4;
};

inline m256d operator-(m256d a, const m256d& b)
{
  a -= b;
  return a;
}

inline m256d operator+(m256d a, const m256d& b)
{
  a += b;
  return a;
}

inline m256d operator*(m256d a, const m256d& b)
{
  a *= b;
  return a;
}

inline m256d operator/(m256d a, const m256d& b)
{
  a /= b;
  return a;
}

#endif
