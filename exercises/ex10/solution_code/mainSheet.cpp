#include <fstream>
#include <sstream>
#include <iomanip>
#include "common.h"
#include "ArrayOfParticles.h"
#include "VelocitySolverNSquared.h"

//#define WEAK_SCALING

using namespace std;


void WriteTimeToFile(const int Nranks, const double time, const char * sFileName)
{
    ofstream outfile;
    outfile.open(sFileName, ios::out | ios::app);
    outfile << Nranks << "    " << scientific << setprecision(6) <<  time << "\n";
    outfile.close();
}

void Dump(ArrayOfParticles& dstParticles, ArrayOfParticles& allParticles, const int Np, const int NpProcess, const int step, const int rank)
{
    // need to gather all particles here
    MPI_Gather(dstParticles.x    , NpProcess, MPI_DOUBLE, &allParticles.x[rank*NpProcess]    , NpProcess, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Gather(dstParticles.y    , NpProcess, MPI_DOUBLE, &allParticles.y[rank*NpProcess]    , NpProcess, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Gather(dstParticles.u    , NpProcess, MPI_DOUBLE, &allParticles.u[rank*NpProcess]    , NpProcess, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Gather(dstParticles.v    , NpProcess, MPI_DOUBLE, &allParticles.v[rank*NpProcess]    , NpProcess, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Gather(dstParticles.gamma, NpProcess, MPI_DOUBLE, &allParticles.gamma[rank*NpProcess], NpProcess, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    if (rank==0)
    {
      char filename[500];
      sprintf(filename, "vortexline_%5.5d.csv", step);

     const double zero=0.0;
     ofstream outfile;
     outfile.open(filename);
     outfile << "x,y,z,vx,vy,vz,g\n";
     outfile << scientific << setprecision(6);
     for (int i=0; i<Np;i++)
     {
       outfile << allParticles.x[i] << ",";
       outfile << allParticles.y[i] << ",";
       outfile << zero << ",";
       outfile << allParticles.u[i] << ",";
       outfile << allParticles.v[i] << ",";
       outfile << zero << ",";
       outfile << allParticles.gamma[i];
       outfile << "\n";
     }

      outfile.close();      
    }
}

void DumpMPI(ArrayOfParticles& dstParticles, const int NpProcess, const int step, const int rank)
{
    char filename[500];
    sprintf(filename, "mpi_vortexline_%5.5d.csv", step);

    MPI_File f;
    MPI_File_open(MPI_COMM_WORLD, filename , MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &f);

    MPI_File_set_size (f, 0);

    MPI_Offset base;
    MPI_File_get_position(f, &base);

    std::stringstream ss;
    // only rank 0 writes the header
    if (rank==0)
      ss << "x,y,z,vx,vy,vz,g\n";

    // all ranks write data
    const double zero=0.0;
    ss << scientific << setprecision(6);
    for (int i = 0; i < NpProcess; ++i)
    {
      ss << dstParticles.x[i] << ",";
      ss << dstParticles.y[i] << ",";
      ss << zero << ",";
      ss << dstParticles.u[i] << ",";
      ss << dstParticles.v[i] << ",";
      ss << zero << ",";
      ss << dstParticles.gamma[i];
      ss << "\n";
    }

    string content = ss.str();

    MPI_Offset len = content.size();
    MPI_Offset offset = 0;
    MPI_Exscan(&len, &offset, 1, MPI_OFFSET, MPI_SUM, MPI_COMM_WORLD);

    MPI_Status status;

    MPI_File_write_at_all(f, base + offset, const_cast<char *>(content.c_str()), len, MPI_CHAR, &status);

    MPI_File_close(&f);
}

int main (int argc, char ** argv)
{
    MPI_Init(&argc,&argv);
    
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    // number of processes
    if (rank==0)
      std::cout << "Running with " << size << " MPI processes\n";
    
    // timer setup
    Timer timerIC, timerSim;
	
    // time integration setup
    const double dt = 0.001;
    const double tfinal = 2.5;
    const int ndump = 10;

    // output
    const bool bAnimation = true;
    const bool bVerbose = true;
    const bool dump_mpi = true;

    // number of particles
    const int N = 10000;
#ifndef WEAK_SCALING
    size_t Np = (N/size)*size; // ensures that the number of particles is divisible by the number of workers
#else
    size_t Np = N*sqrt(size);  // keep number of work per rank fixed
#endif
   
    timerIC.start();
    // initialization 
    // each worker gets a part of the whole array
    size_t NpProcess = Np/size;
    // particle vectors
    // dstParticles: particles owned by rank
    // srcParticles: particles with which the interaction has to be computed
    ArrayOfParticles dstParticles(NpProcess), srcParticles(NpProcess);
    ArrayOfParticles allParticles(rank==0 ? Np : 0); // list of all particles for output
    
    const double dx = 1./Np;
    double totGamma=0.; // total circulation is sum over gamma of all particles
    const double Gamma_s = 1.;
    
    // initialize particles: position and circulation
    for (size_t i=0; i<NpProcess; i++)
    {
      const double x = -0.5 + ((double)i+(double)rank*(double)NpProcess+.5)*dx;

      dstParticles.x[i] = x;
      dstParticles.y[i] = 0.0;
      const double g = 4.*Gamma_s*x/sqrt(1.-4.*x*x);
      dstParticles.gamma[i] = g*dx;
        
      totGamma += dstParticles.gamma[i];
        
      // each rank has also to compute the interactions with its own particles
      srcParticles.x[i] = dstParticles.x[i];
      srcParticles.y[i] = dstParticles.y[i];
      srcParticles.gamma[i] = dstParticles.gamma[i];
    }
    MPI_Reduce(rank==0 ? MPI_IN_PLACE : &totGamma, &totGamma, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    timerIC.stop();

    if (rank==0)
    {
      std::cout << "Number of particles: " << Np << std::endl;
      std::cout << "Number of particles per process: " << NpProcess << std::endl;
      std::cout << "Initial circulation: " << totGamma << std::endl;
      std::cout << "IC time: " << timerIC.get_timing() << std::endl;
    }
    
    // initialize velocity solver
    VelocitySolverNSquared VelocitySolver(dstParticles, srcParticles, rank, size);
	
    timerSim.start();
    double t=0;
    int it=0;
    for (it=1; it<=std::ceil(tfinal/dt); it++)
    {
      // reset particle velocities
      dstParticles.ClearVelocities(); 
      // compute velocities corresponding to time n
      VelocitySolver.ComputeVelocity();
        
      // dump the particles
      if ((it-1)%ndump==0 && bAnimation)
      {
        if (!dump_mpi)
          Dump(dstParticles,allParticles,Np,NpProcess,it-1,rank);
        else
          DumpMPI(dstParticles,NpProcess,it-1,rank);
      }
        
      // update time
      if (rank==0)
        t += dt;
        
      MPI_Bcast(&t, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        
      if (it%ndump==0 && rank==0 && bVerbose)
        std::cout << "Iteration " << it << " time " << t << std::endl;
		
      // advance particles in time to n+1 using forward Euler
      dstParticles.AdvectEuler(dt);
      // update "source" particles
      srcParticles = dstParticles;
    }
    // dump final state
    if ((it-1)%ndump==0 && bAnimation)
    {
      dstParticles.ClearVelocities();
      // compute velocities corresponding to time n
      VelocitySolver.ComputeVelocity();

      if (!dump_mpi)
        Dump(dstParticles,allParticles,Np,NpProcess,it-1,rank);
      else
        DumpMPI(dstParticles,NpProcess,it-1,rank);
    }
    
    if (bVerbose)
      std::cout << "Bye from rank " << rank << std::endl;
    timerSim.stop();
    
    if (rank==0)
    {
      char buf[500];
      sprintf(buf, "timing.dat");
      WriteTimeToFile(size,timerSim.get_timing(),buf);
      std::cout << "#Ranks, Time - " << size << "\t" << timerSim.get_timing() << "\t( " << VelocitySolver.timeT << "\t" << VelocitySolver.timeC << " )\n";
    }

    MPI_Finalize();

    return 0;
}


