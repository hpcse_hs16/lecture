from math import pi
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

def isfloat(x):
    try:
        float(x)
        return True
    except:
        return False


data = np.loadtxt('integral.dat', unpack=True)

x = data[0]
y = data[1]
y_errors = data[2]

# === approximations

plt.xscale('log')
plt.yscale('linear')

current_plot ,= plt.plot(x,y,'-o',markersize=4.0)
plt.errorbar(x,y,yerr=y_errors,fmt='o',markersize=4.0,color=current_plot.get_color())

plt.axhline(y = 0.3, linestyle='dashed', color='k')

plt.ylabel(r'$I$',fontsize=16)

plt.xlabel(r'$N$',fontsize=16)

plt.savefig('integral.pdf',dpi=600)

plt.show()

# === error convergence

plt.xscale('log')
plt.yscale('log')

current_plot ,= plt.plot(x,y_errors,'-o',markersize=4.0)

plt.ylabel(r'$\Delta I$',fontsize=16)

plt.xlabel(r'$N$',fontsize=16)

plt.savefig('integral_errors.pdf',dpi=600)

plt.show()

# === scaling

#directly parse file
x = []
y = []
error= []
with open("scaling.dat") as f:
    for line in f:
        line=line.strip().split()
        if(len(line)!=0):
            if(line[0]=="N:"):
                x.append([])
                y.append([])
                error.append([])
            elif(isfloat(line[0])):
                x[-1].append(int(line[0]))
                y[-1].append(float(line[1]))
                error[-1].append(float(line[2]))


plt.xscale('linear')
plt.yscale('linear')

##change color
cmap = mpl.cm.rainbow

#optimal scaling
optimal = np.arange(1,25,1)
plt.plot(optimal,optimal,label='optimal', color="black")


#now plot each line
for i in range(0,len(y),2):
    y[i] = y[i][0] /np.array(y[i])
    print(y[i])
    plt.plot(x[i],y[i],label='2^%i'%(i+1), color=cmap(i/float(len(y))))


plt.legend (loc = 'best')
xmin, xmax = plt.xlim()
plt.ylim( (xmin, xmax) )

plt.ylabel(r'$T_1/T_N$',fontsize=16)

plt.xlabel(r'#threads',fontsize=16)

plt.savefig('scaling.pdf',dpi=600)

plt.show()

