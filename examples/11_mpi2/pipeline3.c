// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <stdio.h>
#include <mpi.h>
#include <unistd.h>

//#define MPI_Send MPI_Ssend

#ifndef SLEEPTIME
#define SLEEPTIME 1
#endif

void stage0();
void stage(int i);

double T0;
int main(int argc, char *argv[])
{
   int rank;

   MPI_Init(&argc,&argv);
   MPI_Comm_rank(MPI_COMM_WORLD,&rank);

   MPI_Barrier(MPI_COMM_WORLD);
   T0 = MPI_Wtime();

   if (rank == 0)
	stage0();
   else
	stage(rank);

   MPI_Barrier(MPI_COMM_WORLD);
   MPI_Finalize();
   return 0;
}

void stage0()
{
   double x[10];
   int size;
   MPI_Comm_size(MPI_COMM_WORLD,&size);

   for (int i = 0; i < 10; i++)
   {
	x[i] = i+1;
	sleep(SLEEPTIME);	
	printf("stage 0, input %d: %lf\n", i, x[i]);
	MPI_Send(&x[i],1,MPI_DOUBLE,1,99,MPI_COMM_WORLD);
   }

   double zero = 0;
   MPI_Send(&zero,1,MPI_DOUBLE,1,99,MPI_COMM_WORLD);

   printf("[T=%lf] stage 0 exiting ...\n", MPI_Wtime()-T0);

}

void stage(int i)
{
   double x;
   MPI_Status status;
   int size;
   MPI_Comm_size(MPI_COMM_WORLD,&size);

   int counter = 0;
   while (1)
   {
	MPI_Recv(&x,1,MPI_DOUBLE,i-1,99,MPI_COMM_WORLD,&status);
	x *= 2;
	sleep(SLEEPTIME);
	if(i < size-1) {
		MPI_Send(&x,1,MPI_DOUBLE,i+1,99,MPI_COMM_WORLD);
        }
	else {
		printf("[T=%lf] stage %d input %d: %lf\n", MPI_Wtime()-T0, i, counter, x);
		counter++;
	}

	if (x == 0) { 
		printf("[T=%lf] stage %d exiting\n", MPI_Wtime()-T0, i);
		break;
	}
   }
}
