// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <mpi.h>
#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv)
{
	int rank, size;
	int tag1 = 99;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	if (rank == 0) {
		int x = 5;
		MPI_Send(&x, 1, MPI_INT, 1, tag1, MPI_COMM_WORLD);
	} 
	else if (rank == 1) {
		MPI_Request req;
		MPI_Status stat;

		int y;
		MPI_Irecv(&y, 1, MPI_INT, 0, tag1, MPI_COMM_WORLD, &req);
		/* do something useful here */
		MPI_Wait(&req, &stat);

		printf("y = %d\n", y);
	}

	MPI_Finalize();
	return 0;
}
