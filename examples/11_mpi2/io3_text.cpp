// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <mpi.h>
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>


using namespace std;

int main(int argc, char **argv)
{
	int rank;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);


	const int nlocal = 10;
	double data[nlocal];
	for (int i = 0; i < nlocal; i++) data[i] = 100*rank + i;

	int step = 0;
	char filename[265];

	sprintf(filename, "mydata_%05d.txt", step);

	MPI_File f;
	MPI_File_open(MPI_COMM_WORLD, filename , MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &f);

	MPI_File_set_size (f, 0);
	MPI_Offset base;
	MPI_File_get_position(f, &base);

	std::stringstream ss;
	ss << rank << " ";
	for(int i = 0; i < nlocal; ++i)
		ss << data[i] << " ";
	ss << "\n";

	string content = ss.str();

	MPI_Offset len = content.size();
	MPI_Offset offset = 0;
	MPI_Exscan(&len, &offset, 1, MPI_OFFSET, MPI_SUM, MPI_COMM_WORLD);

	MPI_Status status;

	MPI_File_write_at_all(f, base + offset, const_cast<char *>(content.c_str()), len, MPI_CHAR, &status);

	MPI_File_close(&f);

	MPI_Finalize();

	return 0;
}
