// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <iostream>
#include <string>
#include <mpi.h>

#ifndef N 
#define N 1024
#endif

int main(int argc, char** argv) {
  MPI_Status status;
  int num;
  
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD,&num);

  char *ds=(char *)calloc(1, N*sizeof(char)); // to send
  char *dr=(char *)calloc(1, N*sizeof(char)); // to recv

  int tag=99;
  
  if(num==0) {
    MPI_Send(ds,N,MPI_CHAR,1,tag,MPI_COMM_WORLD);
    MPI_Recv(dr,N,MPI_CHAR,1,tag,MPI_COMM_WORLD,&status);
  }
  else {
    MPI_Send(ds,N,MPI_CHAR,0,tag,MPI_COMM_WORLD);
    MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status); 
    int count;
    MPI_Get_count(&status,MPI_CHAR,&count); 
    printf("status.MPI_SOURCE = %d, status.MPI_TAG = %d, count = %d\n", 
		status.MPI_SOURCE, status.MPI_TAG, count);

    MPI_Recv(dr,N,MPI_CHAR,status.MPI_SOURCE,status.MPI_TAG,MPI_COMM_WORLD,&status);
  }
  
  MPI_Finalize();
  return 0;
}
