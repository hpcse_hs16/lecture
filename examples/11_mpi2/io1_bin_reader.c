// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv)
{
	int rank;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int rank_to_print = 0;
	if (argc == 2) rank_to_print = atoi(argv[1]);

	const int nlocal = 10;
	double data[nlocal];

	int step = 0;
	char filename[256];

	sprintf(filename, "mydata_%05d.bin", step);

	MPI_File f;
	MPI_File_open(MPI_COMM_WORLD, filename , MPI_MODE_RDONLY, MPI_INFO_NULL, &f);

	MPI_Offset base;
	MPI_File_get_position(f, &base);

	MPI_Offset len = nlocal*sizeof(double);
	MPI_Offset offset = rank*len;
	MPI_Status status;

	MPI_File_read_at_all(f, base + offset, data, nlocal, MPI_DOUBLE, &status);

	MPI_File_close(&f);

	if (rank == rank_to_print)
		for (int i = 0; i < nlocal; i++) {
			printf("data[%d] = %f\n", i, data[i]);
	}

	MPI_Finalize();
}
