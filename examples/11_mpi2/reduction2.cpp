// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <mpi.h>
#include <iostream>
#include <iomanip>
#include <unistd.h>
using namespace std; 

int main(int argc, char** argv)
{
  MPI_Init(&argc,&argv);
  
  int rank, size;
  MPI_Status status;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);

  
unsigned long const nterms = 100000000;
long double const step = (nterms+0.5l) / (size-1);

// now collect all to the master (rank 0)
if (rank==0) {
    long double localsum=0.;
    long double sum=0.;
    
    // Master receives from all other ranks
    for (int i=1; i<size;++i) {
        MPI_Recv(&localsum, 1, MPI_LONG_DOUBLE, i, 42, MPI_COMM_WORLD,&status);
        sum += localsum;
    }
    
    std::cout << "pi=" << std::setprecision(18) << 4.*sum << std::endl;
    
}
else {
    
    long double localsum=0.;
    // do just one piece on each rank
    unsigned long start = (rank-1) * step;
    unsigned long end = (rank) * step;
    for (std::size_t t = start; t < end; ++t)
        localsum += (1.0 - 2* (t % 2)) / (2*t + 1);
        
    MPI_Send(&localsum, 1, MPI_LONG_DOUBLE, 0, 42, MPI_COMM_WORLD);
}

  MPI_Finalize();
  return 0;
}
