// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void init_data(double *x, int N)
{
	int i;
	printf("initializing %d elements\n", N);
	for (i = 0; i < N; i++) x[i] = 1000 + i;
}

void print_data(double *x, int N)
{
	int i;

	printf("printing %d elements\n", N);
	for (i = 0; i < N; i++)
		printf("x[%d] = %f\n", i, x[i]);
}

int main(int argc, char **argv)
{
	int rank, nranks;
	MPI_Status status;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nranks);

	int MAX_N = 100;
	int N;

	if (rank == 0) {
		N = lrand48()%MAX_N;
		double *x = (double *)calloc(1, N*sizeof(double));
		init_data(x, N);
		MPI_Send(x, N, MPI_DOUBLE, 1, 123, MPI_COMM_WORLD);
	}
	if (rank == 1) {
		double *y = (double *)calloc(1, MAX_N*sizeof(double));
		MPI_Recv(y, MAX_N, MPI_DOUBLE, 0, 123, MPI_COMM_WORLD, &status);
		MPI_Get_count(&status, MPI_DOUBLE, &N);
		print_data(y, N);
	}

	MPI_Finalize();
	return 0;
}
