// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <mpi.h>
#include <iostream>
#include <iomanip>
#include <unistd.h>
using namespace std; 

int main(int argc, char** argv)
{
  MPI_Init(&argc,&argv);
  
  int rank, size;
  MPI_Status status;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);

unsigned long const nterms = 100000000;
int ntasks = 2*(size-1);
long double const step = (nterms+0.5l) / ntasks;

if (rank==0) {
    long double localsum=0.;  
    long double sum=0.;
    
    // Master receives from all other ranks
    for (int i=0; i<ntasks;++i) {
        MPI_Recv(&localsum, 1, MPI_LONG_DOUBLE, MPI_ANY_SOURCE, 42, MPI_COMM_WORLD,&status);
        sum += localsum;
    }
    
    std::cout << "pi=" << std::setprecision(18) << 4.*sum << std::endl;
    
}
else {
    
    for (int i = 0; i < 2; i++)
    {
        long double localsum=0.;
        int taskid = (rank-1) + i*(size-1);
        
        unsigned long start = (taskid) * step;
        unsigned long end = (taskid+1) * step;
        for (std::size_t t = start; t < end; ++t)
            localsum += (1.0 - 2* (t % 2)) / (2*t + 1);
        
        MPI_Send(&localsum, 1, MPI_LONG_DOUBLE, 0, 42,MPI_COMM_WORLD);
    }
    
}

  MPI_Finalize();
  return 0;
}
