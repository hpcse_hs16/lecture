// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <stdio.h>
#include <math.h>
#include <mpi.h>

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);
    
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    if (rank % 2)
        MPI_Barrier(MPI_COMM_WORLD);
    
    MPI_Finalize();
    return 0;
}
