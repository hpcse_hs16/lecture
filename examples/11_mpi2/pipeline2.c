// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <stdio.h>
#include <mpi.h>

void stage0();
void stage(int i);

int main(int argc, char *argv[])
{
   int rank;

   MPI_Init(&argc,&argv);
   MPI_Comm_rank(MPI_COMM_WORLD,&rank);

   if (rank == 0)
	stage0();
   else
	stage(rank);

   MPI_Finalize();
   return 0;
}

void stage0()
{
   double x[10], y[10];
   MPI_Status status;
   int size;
   MPI_Comm_size(MPI_COMM_WORLD,&size);


   for (int i = 0; i < 10; i++)
   {
	x[i] = i+1;
	printf("stage 0: %lf\n", x[i]);
	MPI_Send(&x[i],1,MPI_DOUBLE,1,99,MPI_COMM_WORLD);
	MPI_Recv(&y[i],1,MPI_DOUBLE,size-1,99,MPI_COMM_WORLD,&status);
   }

   for (int i = 0; i < 10; i++)
	printf("x[%d] = %lf -> y[%d] = %lf\n", i, x[i], i, y[i]);

    double zero = 0, last;
    MPI_Send(&zero,1,MPI_DOUBLE,1,99,MPI_COMM_WORLD);
    MPI_Recv(&last,1,MPI_DOUBLE,size-1,99,MPI_COMM_WORLD,&status);
}

void stage(int i)
{
   double x;
   MPI_Status status;
   int size;
   MPI_Comm_size(MPI_COMM_WORLD,&size);

   while (1)
   {
	MPI_Recv(&x,1,MPI_DOUBLE,i-1,99,MPI_COMM_WORLD,&status);
	x *= 2;
	printf("stage %d: %lf\n", i, x);
	if(i < size-1)
		MPI_Send(&x,1,MPI_DOUBLE,i+1,99,MPI_COMM_WORLD);
	else
		MPI_Send(&x,1,MPI_DOUBLE,0,99,MPI_COMM_WORLD);

	if (x == 0) break;
   }
}
