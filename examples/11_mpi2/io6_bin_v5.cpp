// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct header_s
{
	int nlocal;
	MPI_Offset offset;
} header_t;


int main(int argc, char **argv)
{
	int rank, nranks;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nranks);

	srand48(rank);

	const int nlocal = lrand48()%20;
	double data[nlocal];

	for (int i = 0; i < nlocal; i++) data[i] = 100*rank + i;

	printf("rank %d: nlocal = %d\n", rank, nlocal);
	for (int i = 0; i < nlocal; i++) {
		printf("rank %d: data[%d] = %f\n", rank, i, data[i]);
	}

	int step = 0;
	char filename[256];

	sprintf(filename, "mydata_%05d.bin", step);

	MPI_File f;
	MPI_File_open(MPI_COMM_WORLD, filename , MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &f);

	MPI_Offset ntotal = 0;
	MPI_Offset nbytes = sizeof(header_t) + nlocal*sizeof(double);
	printf("nbytes = %lld\n", nbytes);

	MPI_Allreduce(&nbytes, &ntotal, 1, MPI_OFFSET, MPI_SUM, MPI_COMM_WORLD);
	printf("ntotal = %lld\n", ntotal);
	MPI_File_preallocate(f, ntotal);

	MPI_Offset base;
	MPI_File_get_position(f, &base);
	printf("base = %lld\n", base);

	MPI_Offset len = nlocal*sizeof(double);
	MPI_Offset offset = 0;
	MPI_Exscan(&len, &offset, 1, MPI_OFFSET, MPI_SUM, MPI_COMM_WORLD);

	MPI_Status status;

	header_t info;

	info.nlocal = nlocal;
	info.offset = offset;

	MPI_File_write_at_all(f, base + rank*sizeof(info), &info, sizeof(info), MPI_CHAR, &status);
	base = base + nranks * sizeof(info); 

	MPI_File_write_at_all(f, base + offset, data, nlocal, MPI_DOUBLE, &status);

	MPI_File_close(&f);

	MPI_Finalize();

	return 0;
}
