// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <stdio.h>
#include <math.h>
#include <mpi.h>

#ifndef NITERS
#define NITERS 10000
#endif

int main(int argc, char **argv)
{
	int rank, size;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	const int n_iters = NITERS;
	double t, elapsed;
	int i;

	/* do performance benchmark */
	if (rank == 0) printf("Entering MPI_Barrier() benchmark\n");

	MPI_Barrier(MPI_COMM_WORLD);
	t = MPI_Wtime();
	for (i=0; i<n_iters; i++) MPI_Barrier(MPI_COMM_WORLD);
	elapsed = MPI_Wtime() - t;

	MPI_Barrier(MPI_COMM_WORLD);
	if (rank == 0) printf("MPI_Barrier() benchmark: Average Barrier Latency = %lf usec\n", (elapsed/(double)n_iters)*1000.0*1000.0);

	MPI_Finalize();
	return 0;
}
