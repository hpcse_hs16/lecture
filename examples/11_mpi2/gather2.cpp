// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <mpi.h>
#include <vector>
#include <stdio.h>
#include <numeric>
#include <iostream>

using namespace std;

int main( int argc, char** argv )
{
    // vector size
    const int N = 1600;
   
    int num_processes, rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD,&num_processes);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    
    // initialize local parts of the vectors and do the sum z = x + y
    int nlocal = N / num_processes;
    std::vector<float> x(nlocal,-1.2), y(nlocal,3.4), z(nlocal);
    for( int i = 0; i < nlocal; i++ ) z[i] = x[i] + y[i];

    if (rank == 0) {     
        std::vector<float> fullz(N);
        MPI_Gather(&z[0],nlocal,MPI_FLOAT,&fullz[0],nlocal,MPI_FLOAT, 0,MPI_COMM_WORLD);

        std::cout << std::accumulate( fullz.begin(), fullz.end(), 0. ) << std::endl;   
     } 
     else {
        MPI_Gather(&z[0],nlocal,MPI_FLOAT,NULL,nlocal,MPI_FLOAT, 0,MPI_COMM_WORLD);
     } 

    MPI_Finalize();

    return 0;
}
