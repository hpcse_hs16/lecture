// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <stdio.h>
#include <mpi.h>

void stage0();
void stage(int i);

int main(int argc, char *argv[])
{
   int rank;

   MPI_Init(&argc,&argv);
   MPI_Comm_rank(MPI_COMM_WORLD,&rank);

   if (rank == 0)
	stage0();
   else
	stage(rank);

   MPI_Finalize();
   return 0;
}

void stage0()
{
   double x, y;
   MPI_Status status;
   int size;
   MPI_Comm_size(MPI_COMM_WORLD,&size);

   scanf("%lf",&x);
   printf("stage 0: %lf\n", x);
   MPI_Send(&x,1,MPI_DOUBLE,1,99,MPI_COMM_WORLD);
   MPI_Recv(&y,1,MPI_DOUBLE,size-1,99,MPI_COMM_WORLD,&status);
   printf("x = %lf -> y = %lf\n", x, y);
}

void stage(int i)
{
   double x;
   MPI_Status status;
   int size;
   MPI_Comm_size(MPI_COMM_WORLD,&size);

   MPI_Recv(&x,1,MPI_DOUBLE,i-1,99,MPI_COMM_WORLD,&status);
   x *= 2;
   printf("stage %d: %lf\n", i, x);
   if(i < size-1)
      MPI_Send(&x,1,MPI_DOUBLE,i+1,99,MPI_COMM_WORLD);
   else
      MPI_Send(&x,1,MPI_DOUBLE,0,99,MPI_COMM_WORLD);
}
