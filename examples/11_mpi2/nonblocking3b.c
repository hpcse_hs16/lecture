// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <mpi.h>
#include <stdio.h>

int main(int argc, char *argv[])  {
 int rank, size;
 int tag1 = 98, tag2 = 99;
 MPI_Request req0, req1;
 MPI_Status stat0, stat1;
  
 MPI_Init(&argc,&argv);
 MPI_Comm_rank(MPI_COMM_WORLD, &rank);
 MPI_Comm_size(MPI_COMM_WORLD, &size);

 int x[65000];
 int y[2][65000];

 if (rank == 0) {
   x[0] = 5; 
   MPI_Isend(&x[0], 65000, MPI_INT, 1, tag1, MPI_COMM_WORLD, &req0);
   x[0] = 10;
   MPI_Isend(&x[0], 65000, MPI_INT, 1, tag2, MPI_COMM_WORLD, &req1);
 } 
 else if (rank == 1) {
   MPI_Irecv(&y[0][0], 65000, MPI_INT, 0, tag1, MPI_COMM_WORLD, &req0);
   MPI_Irecv(&y[1][0], 65000, MPI_INT, 0, tag2, MPI_COMM_WORLD, &req1);
 }

 MPI_Wait(&req0, &stat0);
 MPI_Wait(&req1, &stat1);

 if (rank == 1) printf("y[0][0] = %d, y[1][0] = %d\n", y[0][0], y[1][0]);

 MPI_Finalize();

 return 0;
}
