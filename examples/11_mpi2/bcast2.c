// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc , char **argv)
{
   int rank, size;
   double data;

   MPI_Init(&argc, &argv);
   MPI_Comm_size(MPI_COMM_WORLD, &size);
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);

   srand48(rank);

   for (int k = 0; k < 10; k++) {
      if (!rank) data = drand48();

      if (!rank) {
         for (int i = 1; i < size; i++)
            MPI_Ssend(&data, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
      }
      else {
          sleep(lrand48()%2); // random sleep - what can I do?
          MPI_Status status;
          MPI_Recv(&data, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
      }

      printf("Step %d: I am Process %d Data = %f\n", k, rank, data);
   }

    MPI_Finalize();
}
