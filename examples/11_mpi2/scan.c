// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <mpi.h>
#include <math.h>
#include <stdio.h>

int main(int argc, char **argv){
  int rank;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  double indata = 10.0*(rank+1); 
  double outdata;

  MPI_Scan(&indata, &outdata, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  printf("process %d: %f -> %f\n", rank, indata, outdata);

  MPI_Barrier(MPI_COMM_WORLD);
  if (rank == 0) printf("\n");
  MPI_Barrier(MPI_COMM_WORLD);

  MPI_Exscan(&indata, &outdata, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  printf("process %d: %f -> %f\n", rank, indata, outdata);

  
  MPI_Finalize();

  return 0;
}
