// Example codes for HPC course
// (c) 2016 Panos Hadjidoukas, ETH Zurich

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct header_s
{
	int nlocal;
	MPI_Offset offset;
} header_t;


int main(int argc, char **argv)
{
	int rank, nranks;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nranks);

	srand48(rank);

	int nlocal;
	double *data;

	int step = 0;
	char filename[256];

	sprintf(filename, "mydata_%05d.bin", step);

	MPI_File f;
	MPI_File_open(MPI_COMM_WORLD, filename , MPI_MODE_RDONLY, MPI_INFO_NULL, &f);

	MPI_Offset base;
	MPI_File_get_position(f, &base);

    MPI_Offset offset;
	MPI_Status status;

	header_t info;

	MPI_File_read_at_all(f, base + rank*sizeof(info), &info, sizeof(info), MPI_CHAR, &status);
	nlocal = info.nlocal;
	offset = info.offset;

	data = malloc(nlocal*sizeof(double));

	base = base + nranks * sizeof(info);
	MPI_File_read_at_all(f, base + offset, data, nlocal, MPI_DOUBLE, &status);

	MPI_File_close(&f);

	MPI_Finalize();

	printf("rank %d: nlocal = %d\n", rank, nlocal);
	for (int i = 0; i < nlocal; i++) {
		printf("rank %d: data[%d] = %f\n", rank, i, data[i]);
	}


	return 0;
}
