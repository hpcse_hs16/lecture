if [ "$#" -ne 1 ]; then
    echo "usage: runme.sh port"
    exit 1
fi

# you will need to adjust it - the simplest solution is to 
# put the example3 executable in your $HOME directory. 

./example3 0 falcon $1 &
sleep 1
ssh panda "cd example3; ./example3 1 falcon $1"
