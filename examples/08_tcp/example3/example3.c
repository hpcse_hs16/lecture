#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h> 
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>

/* Source + short tutorial: http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html */

extern int errno;
void error(char *msg)
{
	perror(msg);
	exit(0);
}

int main(int argc, char *argv[])
{
     if (argc < 3) {
         fprintf(stderr,"usage: %s [0|1] server portnumber\n",argv[0]);
         exit(0);
     }

     int rank = atoi(argv[1]);	// 0: server (rank 0), 1: client (rank 1)

     /* Prepare communication */
     int sock;
     if (rank == 0)
     {
	int srv_sock;
	unsigned short port;
	struct sockaddr_in server, from;
	int len, n;
	unsigned int fromlen;

	port = (unsigned short) atoi(argv[3]);
	srv_sock=socket(AF_INET, SOCK_STREAM, 0);
	if (srv_sock < 0) error("Opening socket");
	server.sin_family=AF_INET;
	server.sin_addr.s_addr=INADDR_ANY;
	server.sin_port=htons(port);  
	len=sizeof(server);
	if (bind(srv_sock, (struct sockaddr *)&server, len) < 0) 
		error("binding socket");
	fromlen=sizeof(from);
	if (listen(srv_sock,5) < 0) 
		error("listening");

	sock=accept(srv_sock, (struct sockaddr *)&from, &fromlen);
	if (sock < 0) error("Accepting");
		printf("A connection has been accepted from %s\n", inet_ntoa((struct in_addr)from.sin_addr));
     }
     else
     {
	unsigned short port;
	struct sockaddr_in server;
	struct hostent *hp;
	char buffer[1024];
   
	sock= socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0) error("Opening socket");

	server.sin_family = AF_INET;
	hp = gethostbyname(argv[2]);
	if (hp==NULL) error("Unknown host");
	memcpy((char *)&server.sin_addr,(char *)hp->h_addr, hp->h_length);
	port = (unsigned short)atoi(argv[3]);
	server.sin_port = htons(port);
	if (connect(sock, (struct sockaddr *)&server, sizeof server) < 0)
		error("Connecting");

     }


     /* Main code */
     if (rank == 0)
     {
	int x = 33;
	printf("Server: sending x = %d\n", x);
	int n = send(sock, &x, sizeof(int), 0);
	if (n < sizeof(int)) error("Error writing");
     }
     else
     {
	int y = -1;
	int n = recv(sock, &y, sizeof(int),0);
	if (n < sizeof(int)) error("reading from socket");
	printf("Client: received y = %d\n", y);

	unlink("rank1_output.txt");
	FILE *fp = fopen("rank1_output.txt", "w");
	fprintf(fp, "Client: received y = %d\n", y);
	fclose(fp);
     }


     /* Finalize communication */

     if (close(sock) < 0) error("closing");           

     return 0;
}


