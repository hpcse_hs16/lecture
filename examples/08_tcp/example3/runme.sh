if [ "$#" -ne 1 ]; then
    echo "usage: runme.sh port"
    exit 1
fi

./example3 0 localhost $1  &
sleep 1
./example3 1 localhost $1
