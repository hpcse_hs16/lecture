A very simple TCP client and server example, available
along with a short tutorial at:

http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html

To run it: 
terminal one: ./server 5000
terminal two: ./client localhost 5000

Panos
