/* server.c - creates a connection oriented (stream) Internet server
   for the Unix Operating system. Port is passed in as an argument
   To compile on solaris gcc ... -lnsl -lsocket */

/* Source + short tutorial: http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h> 
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>

#define BUFSIZE 4096
extern int errno;
void error(char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
     int sock, newsock, len, n;
     unsigned int fromlen;
     unsigned short port;
     struct sockaddr_in server, from;
     char buffer[BUFSIZE];
     char *msg = "I Got your message";

     if (argc < 2) {
         fprintf(stderr,"usage %s portnumber\n",argv[0]);
         exit(0);
     }
     port = (unsigned short) atoi(argv[1]);
     sock=socket(AF_INET, SOCK_STREAM, 0);
     if (sock < 0) error("Opening socket");
     server.sin_family=AF_INET;
     server.sin_addr.s_addr=INADDR_ANY;
     server.sin_port=htons(port);  
     len=sizeof(server);
     if (bind(sock, (struct sockaddr *)&server, len) < 0) 
          error("binding socket");
     fromlen=sizeof(from);
     if (listen(sock,5) < 0) 
          error("listening");
     while (1) {
         newsock=accept(sock, (struct sockaddr *)&from, &fromlen);
         if (newsock < 0) error("Accepting");
         printf("A connection has been accepted from %s\n",
                 inet_ntoa((struct in_addr)from.sin_addr));
         n = recv(newsock,buffer,BUFSIZE-1,0);
         printf("Read %d bytes from client\n",n);
         if (n < 1) {
	   error("Reading");
         }
         else {
           buffer[n]='\0';
           printf("Message from client is: %s\n",buffer);
           len = strlen(msg);
           n = send(newsock,msg,len,0);
           if (n < len) error("Error writing");
           if (close(newsock) < 0) error("closing");           
	 }
     } 
     return 0; // we never get here 
}
