/* client.c    Creates Internet stream client for a Unix platform.  
   The name and port number of the server are passed in as arguments.
   To compile on solaris gcc ... -lnsl -lsocket
*/

/* Source + short tutorial: http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h> /* for atoi */
#include <string.h>

char *msg = "Hello from the client";
void error(char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
   int sock, n;
   unsigned short port;
   struct sockaddr_in server;
   struct hostent *hp;
   char buffer[1024];
   
   if (argc != 3) { 
         printf("Usage: %s server port\n", argv[0]);
         exit(1);
   }
   sock= socket(AF_INET, SOCK_STREAM, 0);
   if (sock < 0) error("Opening socket");

   server.sin_family = AF_INET;
   hp = gethostbyname(argv[1]);
   if (hp==NULL) error("Unknown host");
   memcpy((char *)&server.sin_addr,(char *)hp->h_addr,         
          hp->h_length);
   port = (unsigned short)atoi(argv[2]);
   server.sin_port = htons(port);
   if (connect(sock, (struct sockaddr *)&server, sizeof server) < 0)
             error("Connecting");
   n = send(sock, msg, strlen(msg),0);
   if (n < strlen(msg))
             error("Writing to socket");
   n = recv(sock, buffer, 1023,0);
   if (n < 1) error("reading from socket");
   buffer[n]='\0';
   printf("The message from the server is %s\n",buffer);
   if (close(sock) < 0) error("closing");
   printf("Client terminating\n");
   return 0;
}
