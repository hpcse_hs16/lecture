0) Remote access 
https://www1.ethz.ch/id/services/list/comp_raum_stud/linux/remote/index_EN

1) Load modules
module load mpi/openmpi-x86_64

2) Compile
mpicc -o example1 example1.c 

(or make) 

3) Run 

mpirun -n 1 ./example1 
mpirun -n 2 ./example1 
mpirun -n 4 ./example1 
mpirun -n 8 ./example1 

(or  sh ./script)
