To build the examples using cmake, you can type at the command prompt: mkdir build; cd build; cmake -DCMAKE_BUILD_TYPE=Release ..
Required packages: cmake, MPI, BLAS 
For Euler: module load gcc; module load mvapich2 (or open_mpi); module load mkl


To build the examples manually, following this example: mpic++ -o example1 example1.cpp 
