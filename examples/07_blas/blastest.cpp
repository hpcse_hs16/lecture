// on euler, we load BLAS with
//   module load openblas

#include <iostream>
#include <cblas.h>

// print row-major matrix
template<typename T>
void print_matrix(T mp, size_t n) {
    for(size_t i = 0; i < n; ++i) {
        for(size_t j = 0; j < n; ++j)
            std::cout << ' ' <<  mp[i*n+j];
        std::cout << std::endl;
    }
}

int main() {

    const int n = 2;
    const int N = n*n;
    using real_t = double;

    real_t * A = new real_t[N],
           * B = new real_t[N],
           * C = new real_t[N];

    for(size_t i = 0; i < N; ++i) {
        A[i] = 1;
        B[i] = i;
    }

    std::cout << "A=" << std::endl;
    print_matrix(A, n);
    std::cout << "B=" << std::endl;
    print_matrix(B, n);

    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
		n, n, n, 1.0, A, n, B, n, 1.0, C, n);

    std::cout << "C=" << std::endl;
    print_matrix(C, n);

    delete[] A;
    delete[] B;
    delete[] C;

    return 0;
}

